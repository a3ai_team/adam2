# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Region, Host, Service, Application
from inventory.utils import fetch_or_update_inventory
from project.models import Project
from project.filters import ProjectFieldFilter
from .filters import RegionFieldFilter,HostFieldFilter


def update_inventory(modeladmin, request, queryset):
    """
    update inventory for the selected regions
    """
    for region in queryset:
        fetch_or_update_inventory(region)


class RegionAdmin(admin.ModelAdmin):
    readonly_fields = ('update_url_hook',)
    list_display = (u'id', 'name', 'project', 'region_code','get_organisation' ,'created_at', 'updated_at')
    list_filter = (ProjectFieldFilter, 'created_at', 'updated_at')
    search_fields = ('name',)
    actions = [update_inventory]
    update_inventory.short_description = "Update inventory for selected Regions"
    date_hierarchy = 'created_at'

    def get_organisation(self, obj):
        """
        Display Organisation Name on Regions
        """
        return obj.project.organisation

    get_organisation.short_description = 'Organisation'
    get_organisation.admin_order_field = 'project__organisation'

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Project in form created by Logged in User
        """
        if db_field.name == "project" and not request.user.is_superuser:
            kwargs["queryset"] = Project.objects.filter(users=request.user.user)
        return super(RegionAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        query = super(RegionAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(project__users=request.user.user).order_by('-created_at')

admin.site.register(Region, RegionAdmin)


class HostAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Host',{'fields' : ('name','aws_id','region','env','fqdn','ip','host_type','state',)}),
        ('Monitoring and Description', {
            'classes':('grp-collapse','grp-open'),
            'fields': (
                ('monitoring_method','monitoring_url','monitoring_url_secure'),'Description')

         })
    )

    list_display = (
        'name',
        'aws_id',
        'region',
        'fqdn',
        'ip',
        'host_type',
        'env',
        'state',
        'monitoring_method',
        'monitoring_url',
        'Description',
        'created_at',
        'updated_at',
    )
    list_filter = (RegionFieldFilter, 'created_at', 'updated_at', 'env', 'state')
    search_fields = ('name', 'aws_id', 'ip', 'env', 'host_type', 'state')
    date_hierarchy = 'created_at'

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Regions in Host created by Logged in User
        """
        if db_field.name == "region" and not request.user.is_superuser:
            kwargs["queryset"] = Region.objects.filter(project__users__user=request.user)
        return super(HostAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        query = super(HostAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(region__project__users=request.user.user).order_by('-created_at')

admin.site.register(Host, HostAdmin)



class ServiceAdmin(admin.ModelAdmin):
    fieldsets=(
        ('Service Info',{'fields':('name','port','host','fqdn',)}),
        ('Monitoring and Dependency',{'classes':('grp-collapse','grp-closed'),'fields':(('monitoring_url','monitoring_url_secure'),'service_dependency')})
    )
    filter_horizontal=('service_dependency',)
    list_display = (
        'name',
        'host',
        'fqdn',
        'port',
        'monitoring_url',
        'created_at',
        'updated_at',
    )
    list_filter = (HostFieldFilter, 'created_at', 'updated_at')
    search_fields = ('name', 'port', 'fqdn')
    date_hierarchy = 'created_at'

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Host in Service form created by Logged in User
        """
        if db_field.name == "host" and not request.user.is_superuser:
            kwargs["queryset"] = Host.objects.filter(region__project__users=request.user.user)
        return super(ServiceAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        query = super(ServiceAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(host__region__project__users=request.user.user).order_by('-created_at')

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """
        List all service_dependency in Service form created by Logged in User
        """
        if db_field.name == "service_dependency" and not request.user.is_superuser:
            kwargs["queryset"] = Service.objects.filter(host__region__project__users=request.user.user)
        return super(ServiceAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

admin.site.register(Service, ServiceAdmin)



class ApplicationAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Required', { 'fields' : ('name','port','application_server_used','host','fqdn',('monitoring_url','monitoring_url_secure'))}),
        ('Dependencies', {
            'classes': ('grp-collapse','grp-closed'),
            'fields': ('application_dependency',
                'service_dependency',),
        }),
    )
    filter_horizontal=('service_dependency','application_dependency',)
    list_display = (
        'name',
        'port',
        'application_server_used',
        'host',
        'fqdn',
        'monitoring_url',
        'created_at',
        'updated_at',
    )
    list_filter = (HostFieldFilter, 'created_at', 'updated_at')
    search_fields = ('name',)
    date_hierarchy = 'created_at'

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Host in Service form created by Logged in User
        """
        if db_field.name == "host" and not request.user.is_superuser:
            kwargs["queryset"] = Host.objects.filter(region__project__users=request.user.user)
        return super(ApplicationAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_queryset(self, request):
        query = super(ApplicationAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(host__region__project__users=request.user.user).order_by('-created_at')

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """
        List all Application and service_dependency in Application form created by Logged in User
        """
        if db_field.name == "application_dependency" and not request.user.is_superuser:
            kwargs["queryset"] = Application.objects.filter(host__region__project__users=request.user.user)
        if db_field.name == "service_dependency" and not request.user.is_superuser:
            kwargs["queryset"] = Service.objects.filter(host__region__project__users=request.user.user)
        return super(ApplicationAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

admin.site.register(Application, ApplicationAdmin)
