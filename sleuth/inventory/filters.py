from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from .models import Region, Host

class HostFieldFilter(SimpleListFilter):
    """
    Define Filters for host in filter option
    """
    title = 'host'
    parameter_name = 'host'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            host=Host.objects.filter()
        else:
            host = Host.objects.filter(region__project__users=request.user.user)
        return [(h['id'],h['name']) for h in host.values()]

    def queryset(self, request,queryset):
        if self.value():
            return queryset.filter(host__id=self.value())
        else:
            return queryset

class RegionFieldFilter(SimpleListFilter):
    """
    Define Filters for region fields in filter option
    """
    title = 'Region'
    parameter_name = 'Region'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            region=Region.objects.filter()
        else:
            region = Region.objects.filter(project__users=request.user.user)
        return [(r['id'],r['name']) for r in region.values()]

    def queryset(self, request,queryset):
        if self.value():
            return queryset.filter(region__id=self.value())
        else:
            return queryset
