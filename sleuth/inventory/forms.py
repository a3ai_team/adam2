from django import forms
from .models import Region
# place form definition here
class RegionForm(forms.ModelForm):
    """
    form for region such that it can be edited, created and deleted accordingly
    """
    class Meta:
        model=Region
        fields=['name','project','region_code','update_url_hook']
