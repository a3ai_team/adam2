# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-22 09:04
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('inventory', '0005_auto_20160721_1924'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='application_server_used',
            field=models.CharField(choices=[('war', 'war'), ('jetty', 'jetty'), ('jar', 'jar')], max_length=255),
        ),
        migrations.AlterField(
            model_name='host',
            name='host_type',
            field=models.CharField(choices=[('rabbit', 'rabbit'), ('h2', 'h2'), ('mongo', 'mongo'), ('bamboo', 'bamboo'), ('ansible', 'ansible'), ('wowza', 'wowza'), ('docker', 'docker'), ('elastic search', 'elastic search'), ('nginx', 'nginx'), ('hazelcast', 'hazelcast'), ('cassandra', 'cassandra')], max_length=255),
        ),
    ]
