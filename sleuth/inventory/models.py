from __future__ import unicode_literals
from django.db import models
from project.models import Project, Incident
from notifications.models import Trigger
from django.db.models.signals import post_save, pre_delete
from django_extensions.db.fields import UUIDField
from django.contrib.sites.models import Site
from monitor.manager import ValueManagerES
import logging
import json
import uuid

logger = logging.getLogger(__name__)


class Region(models.Model):
    """
    Denotes a region that contains a set of hosts for a project
    """
    name = models.CharField(max_length=255, null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    project = models.ForeignKey(Project, null=True, on_delete=models.CASCADE, related_name="hosts")
    guid = UUIDField(default=uuid.uuid4)
    update_url_hook = models.CharField(max_length=255, null=True, blank=True)
    REGIONS = (
        ('us-east-1', 'Virginia'),
        ('us-west-2', 'Oregon'),
        ('us-west-1', 'California'),
        ('eu-west-1', 'Ireland'),
        ('eu-central-1', 'Frankfurt'),
        ('ap-southeast-1', 'Singapore'),
        ('ap-northeast-1', 'Tokyo'),
        ('ap-southeast-2', 'Sydney'),
        ('ap-northeast-2', 'Seoul'),
        ('ap-south-1', 'Mumbai'),
        ('sa-east-1', 'Sao Paulo'),
    )
    region_code = models.CharField(max_length=255, null=False, blank=False, choices=REGIONS)
    def __unicode__(self):
        return "%s" % (self.name)

def populate_inventory(sender, instance, created, **kwargs):
    """
    updates or creates inventory when a region is added
    """
    if created:
        from .tasks import fetch_or_update_inventory_task
        site = Site.objects.all()[0]
        url = site.domain
        instance.update_url_hook = url+'/update/region/'+str(instance.guid)+'/'
        instance.save()
        fetch_or_update_inventory_task.delay(region=instance)
        logger.debug("New region %s added , populating inventory" % instance)

post_save.connect(populate_inventory, sender=Region)

def update_url_hook_for_region(sender, instance, created, **kwargs):
    """
    update all region hook urls for regions when site url is updated
    """
    site = Site.objects.all()[0]
    url = site.domain
    for region in Region.objects.all():
        region.update_url_hook = url+'/update/region/'+str(region.guid)+'/'
        region.save()

post_save.connect(update_url_hook_for_region, sender=Site)

class Host(models.Model):
    """
    Basic unit of inventory, denotes a server
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    aws_id = models.CharField(max_length=255, null=True, blank=True)
    region =  models.ForeignKey(Region, null=True, on_delete=models.CASCADE, related_name="machines")
    env = models.CharField(max_length=255, null=True, blank=True)
    fqdn = models.CharField(max_length=255, null=True, blank=False)
    state = models.CharField(max_length=255, null=True, blank=False, default='Running')
    ip = models.GenericIPAddressField()
    HOST_TYPES = (
        ('app', 'app'),
        ('rabbit', 'rabbit'),
        ('h2', 'h2'),
        ('mongo', 'mongo'),
        ('bamboo', 'bamboo'),
        ('ansible', 'ansible'),
        ('wowza', 'wowza'),
        ('docker', 'docker'),
        ('elastic search', 'elastic search'),
        ('nginx', 'nginx'),
        ('hazelcast', 'hazelcast'),
        ('cassandra', 'cassandra'),
        ('jenkins', 'jenkins'),
        ('starprofiler', 'starprofiler' ),
        ('logio', 'logio'),
        ('cloudera','cloudera' ),
        ('datasafe', 'datasafe'),
        ('ffmpeg', 'ffmpeg'),
        ('NA', 'NA')
    )
    host_type = models.CharField(max_length=255, null=False, blank=False, choices=HOST_TYPES)
    MONITORING_METHODS = (
        ('url based', 'url based'),
        ('cloudwatch', 'cloudwatch'),
        ('custom', 'custom')
    )
    monitoring_method = models.CharField(max_length=255, null=True, blank=False, choices=MONITORING_METHODS, default='custom')
    monitoring_url = models.CharField(max_length=255, null=True, blank=True)
    monitoring_url_secure = models.BooleanField(default=False)
    Description = models.TextField()
    incidents = models.ManyToManyField(Incident, blank=True, related_name='hosts')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return "%s" % (self.name)


def delete_values_of_deleted_host(sender, instance, **kwargs):
    """
    delete values from elasticsearch associated with the deleted host
    """
    if instance.host_items.exists():
        related_item_list = instance.host_items.values()
        is_item = False         # to create different body for item
        for item in related_item_list:      # if single host is configured in multiple items
            instance_meta = { "model_name" : "Host", "model_id" : instance.id, "item" : item["id"] }
            status, result = ValueManagerES.delete(is_item, instance_meta)
            if status:
                json_result = json.loads(result.text)
                logger.debug("%d values deleted for Host with id %d and name %s" %(json_result["_indices"]["_all"]["deleted"], instance.id, instance.name))
            else:
                logger.debug("Error : - %s" %(result.text))

pre_delete.connect(delete_values_of_deleted_host, sender=Host)


class Service(models.Model):
    """
    Denotes servcies running inside a host
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    port = models.IntegerField(null=True, blank=False)
    host = models.ForeignKey(Host, null=False, on_delete=models.CASCADE, related_name="services")
    fqdn = models.CharField(max_length=255, null=True, blank=True)
    monitoring_url = models.CharField(max_length=255, null=True, blank=True)
    monitoring_url_secure = models.BooleanField(default=False)
    service_dependency = models.ManyToManyField("self", blank=True, related_name='service_dependent_services')
    incidents = models.ManyToManyField(Incident, blank=True, related_name='services')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return "%s" % (self.name)


class Application(models.Model):
    """
    Denotes an application running inside a host
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    port = models.IntegerField(null=True, blank=False)
    APPLICATION_SERVER_TYPES = (
        ('uwsgi', 'uwsgi'),
        ('gunicorn', 'gunicorn'),
        ('tomcat', 'tomcat'),
        ('jboss', 'jboss'),
        ('IIS', 'IIS'),
        ('war', 'war'),
        ('jetty', 'jetty'),
        ('jar', 'jar'),
        ('nodejs', 'nodejs'),
    )
    application_server_used = models.CharField(max_length=255, null=False, blank=False, choices=APPLICATION_SERVER_TYPES)
    host = models.ForeignKey(Host, null=False, on_delete=models.CASCADE, related_name="applications")
    fqdn = models.CharField(max_length=255, null=True, blank=False)
    monitoring_url = models.CharField(max_length=255, null=True, blank=True)
    monitoring_url_secure = models.BooleanField(default=True)
    application_dependency = models.ManyToManyField("self", blank=True, related_name='application_dependent_apps')
    service_dependency = models.ManyToManyField(Service, blank=True, related_name='service_dependent_apps')
    incidents = models.ManyToManyField(Incident, blank=True, related_name='applications')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return "%s" % (self.name)


def delete_values_of_deleted_application(sender, instance, **kwargs):
    """
    delete values from elasticsearch associated with the deleted appplication
    """
    if instance.app_items.exists():
        related_item_list = instance.app_items.values()
        is_item = False         # to create different body for item
        for item in related_item_list:      # if single application is configured in multiple items
            instance_meta = { "model_name" : "Application", "model_id" : instance.id, "item" : item["id"] }
            status, result = ValueManagerES.delete(is_item, instance_meta)
            if status:
                json_result = json.loads(result.text)
                logger.debug("%d values deleted for Application with id %d and name %s" %(json_result["_indices"]["_all"]["deleted"], instance.id, instance.name))
            else:
                logger.debug("Error : - %s" %(result.text))

pre_delete.connect(delete_values_of_deleted_application, sender=Application)
