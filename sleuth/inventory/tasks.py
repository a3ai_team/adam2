import logging
from celery.decorators import periodic_task, task
from celery.task.schedules import crontab
from inventory.utils import fetch_or_update_inventory
from inventory.models import Application, Region

logger = logging.getLogger(__name__)

@task(name= "populate_inventory")
def fetch_or_update_inventory_task(region):
    fetch_or_update_inventory(region)

@periodic_task(run_every=crontab(minute=0, hour='*'))
def update_all_region():
    """
    update all regions exiting in the inventory
    """
    for region in Region.objects.all():
        fetch_or_update_inventory_task.delay(region)
