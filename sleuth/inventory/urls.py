try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
    from django.conf.urls.defaults import *

from .views import inventory_view,application_view,region_list,region_create,region_view,region_edit,region_delete_refresh,secure_url

urlpatterns=[
    url(r'^list/(?P<org>[\w-]+)/(?P<project>[\w-]+)$',inventory_view,name='inventory_view'),
    url(r'^list/all/$',inventory_view,name='inventory_view'),
    url(r'^application/list/(?P<org>[\w-]+)/(?P<project>[\w-]+)/(?P<host>[\w-]+)$',application_view,name='application_view'),
    url(r'^application/list/all/$',application_view,name='application_view'),
    url(r'^region/list/(?P<org>[\w-]+)/(?P<project>[\w-]+)$',region_list,name='region_list'),
    url(r'^region/list/all/$',region_list,name='region_list'),
    url(r'^region/create/(?P<org>[\w-]+)/(?P<project>[\w-]+)$',region_create,name='region_create'),
    url(r'^region/create/$',region_create,name='region_create'),
    url(r'^region/details/(?P<org>[\w-]+)/(?P<project>[\w-]+)/(?P<region>[\w-]+)$',region_view,name='region_view'),
    url(r'^region/edit/(?P<org>[\w-]+)/(?P<project>[\w-]+)/(?P<region>[\w-]+)$',region_edit,name='region_edit'),
    url(r'^regions/delete_refresh/$', region_delete_refresh, name='region_delete_refresh'),
    url(r'^secure_url/(?P<id>[\d-]+)$', secure_url, name='secure_url'),
]
