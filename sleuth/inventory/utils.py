import logging
from datetime import datetime, timedelta
from operator import itemgetter
from boto3.session import Session
from inventory.models import Region, Host, Application
from aliyunsdkcore import client
from aliyunsdkecs.request.v20140526 import DescribeInstancesRequest
import json

logger = logging.getLogger(__name__)

def fetch_or_update_inventory(region):
    """
    fetch instances from ec2 to populate inventory
    accepts region object and project object
    """
    project = region.project
    session = Session(
        aws_access_key_id=project.aws_access_key_id,
        aws_secret_access_key=project.aws_secret_access_key,
        region_name=region.region_code)
    ecs = session.client('ec2')
    instances = ecs.describe_instances()['Reservations']
    logger.debug("fetched latest inventory details")
    current_instances = []
    fetched_instances = []
    for host in Host.objects.filter(region=region):
        current_instances.append(host.aws_id)

    for instance_detail in instances:
        instance = instance_detail['Instances'][0]
        instance_id = instance['InstanceId']
        instance_type = 'NA'
        instance_state = instance['State']['Name']
        if instance_state == 'running':
            fetched_instances.append(instance_id)
            instance_fqdn = None
            try:
                instance_ip = instance['NetworkInterfaces'][0]['Association']['PublicIp']
            except KeyError:
                instance_ip = instance['PrivateIpAddress']
            instance_tags =  instance['Tags']
            apps = []
            for tag in instance_tags:
                if 'app' in tag['Key'].lower():
                    apps.append(tag['Value'])
                elif 'Env' in tag['Key']:
                    instance_env = tag['Value']
                elif 'name' in tag['Key'].lower():
                    instance_name = tag['Value']
                elif 'type' in tag['Key'].lower():
                    instance_type = tag['Value']
            if not instance_type == 'NA':
                if instance_type == 'nginx':
                    instance_monitoring_method = 'url_based'
                else:
                    instance_monitoring_method = 'custom'

                data = {
                        'ip':instance_ip, 'name':instance_name,
                        'host_type':instance_type, 'region':region,
                        'monitoring_method':instance_monitoring_method,
                        'env':instance_env,
                        'state': instance_state
                        }
                host, created = Host.objects.update_or_create(aws_id=instance_id, defaults=data)
                logger.debug(apps)
                if host.host_type == 'docker' or host.host_type == 'app':
                    create_apps(host, apps)
            else:
                logger.debug("No type tag found for %s ignoring.."% instance_name)
        else:
            logger.debug("{0} is not running".format(instance_id))
    # Delete the hosts that are obsolete
    ids_of_hosts_to_deleted = list(set(current_instances)-set(fetched_instances))
    for host_id in ids_of_hosts_to_deleted:
        Host.objects.get(aws_id=host_id).delete()
        logger.debug("deleted obsolete host %s" % host_id)

def fetch_or_update_inventory_alibaba_cloud(region):
    """
    fetch instances from ec2 to populate inventory
    accepts region object and project object
    """
    project = region.project
    cl=client.AcsClient("dsxf366wUNg3xppj","hBJEWi3uUmXcLI7vNqeoakgZtEgCPe","ap-southeast-1")

    #fetching Total no of servers in alibaba cloud
    request_data=DescribeInstancesRequest.DescribeInstancesRequest()
    request_data.set_accept_format('json')
    results=cl.do_action(request_data)
    logger.debug("fetched latest inventory details for alibaba cloud")
    json_converted_data = results.replace("'", "\"")
    instances=json.loads(json_converted_data)

    #setting pagesize no and then fetching data
    request_data=DescribeInstancesRequest.DescribeInstancesRequest()
    request_data.set_accept_format('json')
    request_data.set_PageSize(str(instances['TotalCount']))
    results=cl.do_action(request_data)
    json_converted_data = results.replace("'", "\"")
    instances=json.loads(json_converted_data)

    current_instances = []
    fetched_instances = []
    for host in Host.objects.filter(region=region):
        current_instances.append(host.aws_id)

    for instance in instances['Instances']['Instance']:
        #print instance['InstanceId']
        instance_id=instance['InstanceId']
        instance_type = 'NA'
        instance_state=instance['Status']
        #print instance_state
        if instance_state == 'Running':
            fetched_instances.append(instance_id)
            instance_fqdn = None
            if instance['InstanceNetworkType']=='classic':
                try:
                    instance_ip=instance['PublicIpAddress']['IpAddress'][0]
                except IndexError:
                    instance_ip=instance['InnerIpAddress']['IpAddress'][0]
            else:
                try:
                    instance_ip=instance['EipAddress']['IpAddress']
                except KeyError:
                    instance_ip=instance['VpcAttributes']['PrivateIpAddress']['IpAddress'][0]
            apps = []
            try:
            #fetching Tags
                for tag in instance['Tags']['Tag']:
                    if 'app' in tag['TagKey'].lower():
                        apps.append(tag['TagValue'])
                    elif 'Env' in tag['TagKey']:
                        instance_env = tag['TagValue']
                    elif 'name' in tag['TagKey'].lower():
                        instance_name = tag['TagValue']
                    elif 'type' in tag['TagKey'].lower():
                        instance_type = tag['TagValue']

                if not instance_type == 'NA':
                    if instance_type == 'nginx':
                        instance_monitoring_method = 'url_based'
                    else:
                        instance_monitoring_method = 'custom'
                    data = {
                                'ip':instance_ip, 'name':instance_name,
                                'host_type':instance_type, 'region':region,
                                'monitoring_method':instance_monitoring_method,
                                'env':instance_env,
                                'state': instance_state
                            }
                    host, created = Host.objects.update_or_create(aws_id=instance_id, defaults=data)
                    logger.debug(apps)
                    if host.host_type == 'docker' or host.host_type == 'app':
                        create_apps(host, apps)
                else:
                    logger.debug("No type tag found for %s ignoring.."% instance_name)
            except:
                logger.debug("No tag found for %s "% instance_id)
        else:
            logger.debug("{0} is not running".format(instance_id))

    ids_of_hosts_to_deleted = list(set(current_instances)-set(fetched_instances))
    for host_id in ids_of_hosts_to_deleted:
        Host.objects.get(aws_id=host_id).delete()
        logger.debug("deleted obsolete host %s" % host_id)

def create_apps(host, apps):
    """
    creates/updates apps
    """
    current_fqdns = []
    fetched_fqdns = []
    for applicaton in Application.objects.filter(host=host):
        current_fqdns.append(applicaton.fqdn)
    for app in apps:
        app_data = app.split(',')
        if len(app_data) != 6:
            logger.debug("app info incomplete, exiting.. instance_id: %s" % host.aws_id)
            pass
        else:
            fqdn = app_data[3]
            fetched_fqdns.append(fqdn)
            monitoring_url = fqdn + '/heartbeat'
            data = {
                    'name':app_data[0], 'application_server_used':
                    app_data[2].lower(), 'port':app_data[5], 'host':host,
                    'monitoring_url': monitoring_url
                    }
            application, created = Application.objects.update_or_create(fqdn=fqdn, defaults=data)
            if created:
                logger.debug("created app %s" % application)
            else:
                logger.debug("updated app %s" % application)
    # Delete apps which are obsolete
    fqdns_of_apps_to_be_deleted = list(set(current_fqdns)-set(fetched_fqdns))
    for fqdn in fqdns_of_apps_to_be_deleted:
        Application.objects.get(fqdn=fqdn).delete()
