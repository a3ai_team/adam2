from .models import Region
from .tasks import fetch_or_update_inventory_task
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests
from django.shortcuts import render,redirect,get_object_or_404
from inventory.models import Host,Application
from project.models import Project
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
import logging
from django.http import Http404
from .forms import RegionForm
from organisation.models import Organisation
from django.core.urlresolvers import reverse_lazy
logger = logging.getLogger(__name__)

@csrf_exempt
def update_region_view(request, region_guid=None):
    """
    handles a get request that updates a given region
    """
    if request.method == 'GET' and (region_guid is not None):
        #request.GET.get('key', '')
        region = Region.objects.filter(guid=region_guid)
        if len(region)==0:
            data = {"code":"404", "message":"resouce not found"}
            return JsonResponse(data, status=404)
        else:
            fetch_or_update_inventory_task.delay(region[0])
            data = {"code":"200", "message":"updating the requested region"}
            return JsonResponse(data, status=200)
    elif request.method == 'POST' and (region_guid is not None):
        try:
            SNS_header = request.META['HTTP_X_AMZ_SNS_MESSAGE_TYPE']
        except Exception:
            logger.debug("No SNS header found")
            data = {"code":"400", "message":"bad request"}
            return JsonResponse(data, status=400)
        else:
            if SNS_header == 'SubscriptionConfirmation':
                subscribe_url = request.body['SubscribeURL']
                logger.debug("posted data is {0}".format(request.body))
                logger.debug("hitting subscribe url found in the response")
                requests.get('%s' % subscribe_url)
                data = {"code":"200", "message":"subscription verified successfully"}
                return JsonResponse(data, status=200)
            elif SNS_header == 'Notification':
                region = Region.objects.filter(guid=region_guid)
                fetch_or_update_inventory_task.delay(region[0])
                data = {"code":"200", "message":"updating the requested region"}
                return JsonResponse(data, status=200)
            else:
                logger.debug("unknown response header value detected")
                data = {"code":"400", "message":"bad request"}
                return JsonResponse(data, status=400)


    else:
        data = {"code":"400", "message":"bad request"}
        return JsonResponse(data, status=400)


@login_required
def inventory_view(request,org=None,project=None):
    """
    view inventory data
    """
    if not project==None and not org==None:
        host_list=Host.objects.filter(region__project__pk=project,region__project__organisation=org,region__project__users=request.user.user)
        org=Organisation.objects.get(pk=org)
        project=Project.objects.get(pk=project)
    elif project==None and org==None:
        host_list=Host.objects.filter(region__project__users=request.user.user)
    else:
        raise Http404("Page not available")
    paginator = Paginator(host_list,8)
    page = request.GET.get('page')
    try:
        hosts = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        hosts = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        hosts = paginator.page(paginator.num_pages)
    return render(request,"inventory/inventory.html",{'hosts':hosts,'org':org,'project':project})

@login_required
def application_view(request,org=None,project=None,host=None):
    """
    Application data based on host
    """
    if not project==None and not org==None and not host==None:
        app_list=Application.objects.filter(host__pk=host,host__region__project__pk=project,host__region__project__organisation=org,host__region__project__users=request.user.user)
    elif project==None and org==None and host==None:
        app_list=Application.objects.filter(host__region__project__users=request.user.user)
    else:
        raise Http404("Page not available")
    paginator = Paginator(app_list,8)
    page = request.GET.get('page')
    try:
        apps = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        apps = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        apps = paginator.page(paginator.num_pages)
    return render(request,"inventory/application_view.html",{'apps':apps})


@login_required
def region_list(request,org=None,project=None):
    """
    Create Region for particular project
    """
    if project==None and org==None:
        """
        List all region
        """
        region_list=Region.objects.filter(project__users=request.user.user)
    elif not project==None and not org==None:
        """
        listing on the basis of project
        """
        try:
            project=Project.objects.get(pk=project,users=request.user.user)
            region_list=Region.objects.filter(project__users=request.user.user,project=project)
        except:
            raise Http404("Invalid Page")
    else:
        raise Http404("Invalid Page")
    """
    pagination for regions
    """
    paginator=Paginator(region_list,7)
    page=request.GET.get('page')
    try:
        regions=paginator.page(page)
    except PageNotAnInteger:
        regions=paginator.page(1)
    except EmptyPage:
        regions=paginator.page(paginator.num_pages)
    return render(request,"inventory/region_list.html",{'regions':regions,'project':project,'org':org})

@login_required
def region_create(request,org=None,project=None):
    """
    create region for specific project
    """
    form=RegionForm(request.POST or None)

    if project==None and org==None:
        """
        if region is created from sidebar
        """
        projects=Project.objects.filter(users=request.user.user)
        form.fields['project'].queryset=projects
        orgs=Organisation.objects.filter(users=request.user.user)
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('region_list'))
    elif not project==None and not org==None:
        """
        if region is created from going through project
        """
        try:
            project=Project.objects.get(users=request.user.user,pk=project)
        except:
            logger.debug("User trying to access others project or project is created yet")
            raise Http404("Invalid access to a project")
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('region_list',args=[org,project.id]))
    else:
        logger.debug("trying to access wrong url")
        raise Http404("Invalid Page")
    return render(request,"inventory/region_form.html",{'form':form ,'org':org,'project':project,'edit':0,'view':0})

@login_required
def region_view(request,org,project,region):
    """
    Read region information
    """
    try:
        org_name=Organisation.objects.get(pk=org)
    except:
        raise Http404("Invalid Page")
    org_list=Organisation.objects.filter(users=request.user.user)
    if org_name in org_list:
        #check whether project is available or not
        project=get_object_or_404(Project,pk=project,users=request.user.user)
        region=get_object_or_404(Region,pk=region,project__users=request.user.user)
        form=RegionForm(request.POST or None, instance=region)
    return render(request,"inventory/region_form.html",{'form':form ,'org':org_name,'view':1,'edit':0,'project':project,'region':region})

@login_required
def region_edit(request,org,project,region):
    """
    Read region information
    """
    try:
        org_name=Organisation.objects.get(pk=org)
    except:
        raise Http404("Invalid Page")
    org_list=Organisation.objects.filter(users=request.user.user)
    if org_name in org_list:
        #check whether project is available or not
        project=get_object_or_404(Project,pk=project,users=request.user.user)
        region=get_object_or_404(Region,pk=region,project__users=request.user.user)
        form=RegionForm(request.POST or None, instance=region)
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('region_list',args=[org,project.id]))
    return render(request,"inventory/region_form.html",{'form':form ,'org':org_name,'view':0,'edit':1,'project':project,'region':region})

@login_required
def region_delete_refresh(request):
    """
    view for deleting project
    """
    org=request.POST.get('org')
    project=request.POST.get('project')
    if request.POST.get('action')=='delete':
        try:
            list_delete=dict(request.POST)['region_selected']
        except:
            list_delete=[]
        regions=Region.objects.filter(pk__in=list_delete)
        if regions is not None:
            if (request.user.is_superuser) or (request.user.user in User.objects.filter()):
                regions.delete()
    elif request.POST.get('action')=='refresh':
        try:
            list_refresh=dict(request.POST)['region_selected']
        except:
            list_refresh=[]
        regions=Region.objects.filter(pk__in=list_refresh)
        for region in regions:
            requests.request("get","http://"+region.update_url_hook)
    if org and project:
        return redirect(reverse_lazy('region_list' ,args=[org,project]))
    return redirect(reverse_lazy('region_list'))

@login_required
def secure_url(request,id):
    app=Application.objects.get(host__region__project__users=request.user.user,pk=id)
    if app.monitoring_url_secure:
        app.monitoring_url_secure=False
    else:
        app.monitoring_url_secure=True
    app.save()
    return redirect(reverse_lazy('application_view'))
