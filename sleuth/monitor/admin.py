# -*- coding: utf-8 -*-
from django.contrib import admin
from django.forms import ModelForm, ValidationError
from inventory.models import Host, Application,Service
from .models import API_check, Item, Value, Scenario, Cron_check
from project.models import Project
from project.filters import ProjectFieldFilter
from .filters import ItemFieldFilter,ApiCheckFieldFilter
from datetime import datetime
from croniter import croniter


class API_checkAdmin(admin.ModelAdmin):
    fieldsets=(
        ('Api Info',{'fields':(('api_name','api_url','api_url_secure'),'env','project','request_type')}),
        ('Request Info',{'fields':('request_headers','request_payload')}),
    )

    list_display = (
        'api_name',
        'api_url',
        'request_type',
        'request_payload',
        'request_headers',
        'created_at',
        'updated_at')
    list_filter = (ProjectFieldFilter,'created_at', 'updated_at')
    date_hierarchy = 'created_at'

    def get_queryset(self, request):
        #filter api's based on logged in user
        query = super(API_checkAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query
        return query.filter(project__users=request.user.user)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Project where user belongs
        """
        if db_field.name == "project" and not request.user.is_superuser:
            kwargs["queryset"] = Project.objects.filter(users=request.user.user)
        return super(API_checkAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(API_check, API_checkAdmin)



def validate_cron_defination(cron_defination):
    """
    checks weather the cron defination specified is correct
    """
    try:
        iter = croniter(cron_defination, datetime.now())
        return True, None
    except Exception as e:
        return False, e.args

class CronForm(ModelForm):
    def clean_cron_defination(self):
        valid,execption=validate_cron_defination(self.cleaned_data['cron_defination'])
        if not valid:
            raise ValidationError("Cron Defination should be in proper Format ( * * * * *)")
        return self.cleaned_data['cron_defination']

class Cron_checkAdmin(admin.ModelAdmin):
    form=CronForm
    fieldsets=(
        (None,{'fields':('name','project','cron_defination','cronhook_target_url','bracktracking_depth')}),
    )
    list_display = (
        'name',
        'cron_defination',
        'cronhook_target_url',
        'bracktracking_depth',
        'created_at',
        'updated_at')

    list_filter = (ProjectFieldFilter,'created_at', 'updated_at')

    readonly_fields = ('cronhook_target_url',)
    date_hierarchy = 'created_at'

    def get_form(self, request, obj=None, **kwargs):
        self.form.request = request
        return super(Cron_checkAdmin, self).get_form(request,**kwargs)

    def get_queryset(self, request):
        #filter crons based on logged in user
        query = super(Cron_checkAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(project__users=request.user.user).order_by('-created_at')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Project where user belongs
        """
        if db_field.name == "project" and not request.user.is_superuser:
            kwargs["queryset"] = Project.objects.filter(users=request.user.user)
        return super(Cron_checkAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(Cron_check, Cron_checkAdmin)


class ItemAdmin(admin.ModelAdmin):

    fieldsets = (
        ('Required', { 'fields' : ('name','project','key','Type','refresh_interval_in_seconds','scenario_counter','is_active')}),
        ('Host', {'classes': ('grp-collapse','grp-closed'),'fields': ('host',)}),
        ('Application',{'fields': ('application',),'classes': ('grp-collapse','grp-closed')}),
        ('Api Checks',{'fields': ('api_check',),'classes': ('grp-collapse','grp-closed')}),
        ('Cron Checks',{'fields': ('cron_check',),'classes': ('grp-collapse','grp-closed')}),
        ('Service',{'fields': ('service',),'classes': ('grp-collapse','grp-closed')}),
    )
    filter_horizontal=('application','host','service')
    list_display = (
        'name',
        'key',
        'Type',
        'refresh_interval_in_seconds',
        'scenario_counter',
        'api_check',
        'created_at',
        'updated_at',
        'is_active',
    )

    list_filter = (ProjectFieldFilter,ApiCheckFieldFilter, 'created_at', 'updated_at', 'is_active')

    search_fields = ('name',)
    date_hierarchy = 'created_at'

    def get_queryset(self, request):
        #filter item's based on project in user
        query = super(ItemAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(project__users=request.user.user).order_by('-created_at')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all foreign key fields where project belongs to some user
        """
        if not request.user.is_superuser:
            if db_field.name == "project" :
                kwargs["queryset"] = Project.objects.filter(users=request.user.user)
            if db_field.name == "api_check" :
                kwargs["queryset"] = API_check.objects.filter(project__users=request.user.user)
            if db_field.name == "cron_check" :
                kwargs["queryset"] = Cron_check.objects.filter(project__users=request.user.user)
        return super(ItemAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        """
        List all Application, service, cron ,host and apis in Item form based on project
        """
        if not request.user.is_superuser:
            if db_field.name == "application":
                kwargs["queryset"] = Application.objects.filter(host__region__project__users=request.user.user)
            if db_field.name == "service" :
                kwargs["queryset"] = Service.objects.filter(host__region__project__users=request.user.user)
            if db_field.name == "host" :
                kwargs["queryset"] = Host.objects.filter(region__project__users=request.user.user)
        return super(ItemAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


admin.site.register(Item, ItemAdmin)


class ValueAdmin(admin.ModelAdmin):
    list_display = (
        'Item',
        'http_response_code',
        'response_time',
        'data',
        'derived_from',
        'created_at',
        'updated_at',
    )
    readonly_fields=('name','model_instance','Item','http_response_code','response_time','data','derived_from',)
    list_filter = ('derived_from','name', 'created_at', 'updated_at')
    search_fields = ('name', 'derived_from')
    date_hierarchy = 'created_at'
    def get_queryset(self, request):
        if request.user.is_superuser:
            return super(ValueAdmin, self).get_queryset(request)
        return Value.objects.filter(Item__project__users=request.user.user).order_by('-created_at')




class ScenarioAdmin(admin.ModelAdmin):
    list_display = (
        'Item',
        'condition',
        'value_to_be_compared_with',
        'number_of_subsequent_tries_after_which_scenario_triggers',
    )

    list_filter = (ItemFieldFilter,)


    def get_form(self, request, obj=None, **kwargs):
        form = super(ScenarioAdmin, self).get_form(request, obj, **kwargs)
        if not request.user.is_superuser:
            form.base_fields['Item'].queryset = Item.objects.filter(project__users=request.user.user)
        return form

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Trigger in form present in projecrs
        """
        if db_field.name == "trigger" and not request.user.is_superuser:
            kwargs["queryset"] = Trigger.objects.filter(project__users=request.user.user)
        return super(ScenarioAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)


    def get_queryset(self, request):
        query = super(ScenarioAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter()
        return query.filter(trigger__project__users=request.user.user)


admin.site.register(Scenario, ScenarioAdmin)
