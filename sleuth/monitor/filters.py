from django.contrib import admin
from .models import API_check,Item
from django.contrib.admin import SimpleListFilter

class ApiCheckFieldFilter(SimpleListFilter):
    """
    Define Filters for ApiCheck in filter option
    """
    title = 'ApiCheck'
    parameter_name = 'ApiCheck'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            api=API_check.objects.filter()
        else:
            api = API_check.objects.filter(project__users=request.user.user)
        return [(a['id'],a['api_name']) for a in api.values()]

    def queryset(self, request,queryset):
        if self.value():
            return queryset.filter(api_check_id=self.value())
        else:
            return queryset


class ItemFieldFilter(SimpleListFilter):
    """
    Define Filters for Items in filter option
    """
    title = 'Item'
    parameter_name = 'Item'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            item=Item.objects.filter()
        else:
            item = Item.objects.filter(project__users=request.user.user)
        return [(i['id'],i['name']) for i in item.values()]

    def queryset(self, request,queryset):
        if self.value():
            return queryset.filter(Item_id=self.value())
        else:
            return queryset
