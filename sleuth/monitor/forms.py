from django import forms
from .models import API_check, Scenario, Cron_check,Item
# place form definition here

class ApiCheckForm(forms.ModelForm):
    """
    Form for API_check model
    """
    class Meta:
        model=API_check
        fields=['api_name','api_url','project','request_type','request_payload','request_headers','api_url_secure','env']

class CronCheckForm(forms.ModelForm):
    """
    Form for Cron Check model
    """
    class Meta:
        model = Cron_check
        fields = ['name','project','cron_defination','bracktracking_depth']

class ScenarioForm(forms.ModelForm):
    """
    Form for Scenario model
    """
    class Meta:
        model = Scenario
        fields = ['name', 'Item', 'trigger', 'condition', 'value_to_be_compared_with', 'number_of_subsequent_tries_after_which_scenario_triggers']

class ItemForm(forms.ModelForm):
    """
    Form for Item model
    """
    class Meta:
        model = Item
        fields = ['name','project','key','Type','refresh_interval_in_seconds','scenario_counter','is_active','api_check','cron_check','host','application','service']
