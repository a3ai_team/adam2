from elasticsearch import Elasticsearch, RequestError, ConnectionError
from django.conf import settings
from datetime import datetime, timedelta
from django.dispatch import Signal
import json
import logging
import requests


logger = logging.getLogger(__name__)
ELASTICSEARCH_URL = getattr(settings, 'ELASTICSEARCH_URL', 'http://localhost:9200/')
value_post_save = Signal(providing_args=["instance", "created"])


def connect_to_elasticsearch():
    return Elasticsearch(ELASTICSEARCH_URL)

class ValueManagerES():
    """
    Manager class to query data from/to Elasticsearch
    """
    index_name_prefix = "value-"
    doc_type = "sleuth"

    @staticmethod
    def __create_template_if_not_exist(es):
        template_name = "value"
        body = {
            "template" : "{0}*".format(ValueManagerES.index_name_prefix),
            "mappings" : {
                ValueManagerES.doc_type : {
                    "properties" : {
                        "item" : { "type" : "integer" },
                        "name" : { "type" : "string" },
                        "http_response_code" : { "type" : "integer" },
                        "response_time" : { "type" : "float" },
                        "data" : { "type" : "string" },
                        "model_name" : { "type" : "string" },
                        "model_id" : { "type" : "integer" },
                        "derived_from" : { "type" : "string" },
                        "organisation" : { "type" : "integer" },
                        "project" : { "type" : "integer" },
                        "datetime" : {
                            "type" : "date",
                            "format" : "yyyy-MM-dd HH:mm:ss.SSS"
                            },
                        "created_at" : { "type" : "date" },
                        "updated_at" : { "type" : "date" },
                    },
               },
           }
        }
        if not es.indices.exists_template(name = template_name):
            try:
                es.indices.put_template(name = template_name, body = body)
            except ConnectionError:
                logger.error("Unable to create template in elasticsearch due to ConnectionError")
            except RequestError:
                logger.error("Unable to create template in elasticsearch due to RequestError")
            logger.debug("%s Template created" %(template_name))

    @staticmethod
    def __create_index_if_not_exist(es, value):
        """
        creates index if it does not exist
        """
        index_name = ValueManagerES.index_name_prefix + value.created_at.strftime("%Y.%m.%d")
        if not es.indices.exists(index = index_name):
            ValueManagerES._ValueManagerES__create_template_if_not_exist(es)
            try:
                es.indices.create(index = index_name)
            except ConnectionError:
                logger.error("Unable to create index in elasticsearch due to ConnectionError")
            except RequestError:
                logger.error("Unable to create index in elasticsearch due to RequestError")
            logger.debug("%s index created" %(index_name))
        return index_name

    @staticmethod
    def __create_document_body(value):
        """
        creates document body used for while creating new document
        """
        body = {
                    "item" : value.Item.pk,
                    "name" : value.name,
                    "http_response_code" : value.http_response_code,
                    "response_time" : value.response_time,
                    "data" : value.data,
                    "model_name" : value.model_name,
                    "model_id" : value.model_id,
                    "derived_from" : value.derived_from,
                    "project" : value.project,
                    "organisation" : value.organisation,
                    "created_at" : value.created_at,
                    "datetime" : value.created_at.strftime("%Y-%m-%d %H:%M:%S.000"),
                    "updated_at" : value.updated_at
            }
        return body

    @staticmethod
    def __create_fetch_body(size, kwargs):
        """
        create search body for fetching value of particular item
        """
        list = []
        dates_dict = {}
        for key, value in kwargs.items():
            if isinstance(value, str):
                value = value.lower()
            if key == "start_time":
                dates_dict['gte']= value.strftime("%Y-%m-%d %H:%M:%S.000")
            if key == "end_time":
                dates_dict['lt']=value.strftime("%Y-%m-%d %H:%M:%S.000")
            if not key == "start_time" and not key == "end_time":
                dict = { "term" : { key : value }}
                list.append(dict)
        body = {
            "query" : {
                "constant_score" : {
                    "filter" : {
                        "bool" : {
                            "must" : list,
                            "filter" : {
                                "range" : {
                                    "datetime" : dates_dict
                                }
                            }
                        }
                    }
                }
            },
            "sort" : { "datetime" : { "order" : "desc"}},
            "size" : size
        }
        return body


    @staticmethod
    def __create_delete_body(is_item, meta_dict):
        """
        create delete body based on type value
        type can be Host or API_check
        """
        if not is_item:
            body = {
                "query" : {
                    "constant_score" : {
                        "filter" : {
                            "bool" : {
                                "must" : [
                                    { "term" : { "item" : meta_dict["item"] }},
                                    { "term" : { "model_name" : meta_dict["model_name"].lower() }},
                                    { "term" : { "model_id" : meta_dict["model_id"] }}
                                ]
                            }
                        }
                    }

                }
             }
            return body
        else:
            body = {
                "query" : {
                    "term" : { "item" : meta_dict["item"]}
                }
            }
            return body

    @staticmethod
    def __generate_index_pattern(start_time, end_time, es):
        """
        finds index pattern on which search query executes
        """
        index_name=""
        index_list = []
        es_index_list = es.indices.get('*')
        diff = abs(end_time - start_time)
        for i in range(diff.days+1):
            index_name = ValueManagerES.index_name_prefix + (start_time + timedelta(days = i)).strftime("%Y.%m.%d")
            if index_name in es_index_list:
                index_list.append(index_name)
        return index_list

    @staticmethod
    def __create_value_objects(list, create_object_list=True):
        """
        creates ValueModel object or ValueModel objects list based on parameter passed
        """
        from .models import ValueModel
        if create_object_list:
            value_list = []
            for list_data in list:
                item = list_data["_source"]["item"]
                name = list_data["_source"]["name"]
                http_response_code = list_data["_source"]["http_response_code"]
                response_time = list_data["_source"]["response_time"]
                data = list_data["_source"]["data"]
                model_name = list_data["_source"]["model_name"]
                model_id = list_data["_source"]["model_id"]
                derived_from = list_data["_source"]["derived_from"]
                project = list_data["_source"]["project"]
                organisation = list_data["_source"]["organisation"]
                created_at = datetime.strptime(list_data["_source"]["created_at"], '%Y-%m-%dT%H:%M:%S+00:00')
                updated_at = datetime.strptime(list_data["_source"]["updated_at"], '%Y-%m-%dT%H:%M:%S+00:00')
                value = ValueModel(item, name, http_response_code, response_time, data, model_name, model_id, derived_from, project, organisation, created_at, updated_at)
                value_list.append(value)
            return value_list
        else:
            value = ValueModel(list[0], list[1], list[2], list[3], list[4], list[5], list[6], list[7], list[8], list[9], list[10], list[11])
            return value

    @staticmethod
    def create_document(item, name, derived_from, data, created_at, http_response_code, model_instance, project, organisation, response_time = None):
        """
        used to insert document in elasticsearch
        """
        updated_at = created_at
        instance_meta = json.loads(model_instance)
        value_data_list = [item, name, http_response_code, response_time, data, instance_meta["model"], instance_meta["id"], derived_from, project, organisation, created_at, updated_at]
        value = ValueManagerES._ValueManagerES__create_value_objects(value_data_list, create_object_list=False)
        body = ValueManagerES._ValueManagerES__create_document_body(value)
        try:
            es = connect_to_elasticsearch()
            index_name = ValueManagerES._ValueManagerES__create_index_if_not_exist(es, value)
            es.create(index = index_name, doc_type = ValueManagerES.doc_type, body = body)
            es.transport.close()
            value_post_save.send(sender=ValueManagerES, instance=value, created=True)
            return value
        except ConnectionError as e:
            logger.error("Unable to create document in elasticsearch due to ConnectionError")
        except RequestError:
            logger.error("")
        return

    @staticmethod
    def fetch(size = 10, latest = False, **kwargs):
        """
        fetch documents(latest or created between particular time frame) from elasticsearch based on arguments passed
        arguments that can be passed are: start_time, end_time, item, model_name, models_id, project, organisation
        To get lastest data for particular project just pass project argument
        """
        body = ValueManagerES._ValueManagerES__create_fetch_body(size, kwargs)         #create search body for searching data in elasticsearch
        try:
            es = connect_to_elasticsearch()
            if kwargs.has_key("start_time") and kwargs.has_key("end_time"):         # create index list if start time and end time is given
               start_time = kwargs["start_time"]
               end_time = kwargs["end_time"]
               index_pattern = ValueManagerES._ValueManagerES__generate_index_pattern(start_time, end_time, es)
            else:                                                                      # create index based on current date
               index_pattern = ValueManagerES.index_name_prefix + datetime.now().date().strftime("%Y.%m.%d")        #creating index to limit search
            result = es.search(index = index_pattern, doc_type = ValueManagerES.doc_type, body = body)
            es.transport.close()
        except ConnectionError:
           logger.error("Unable to fetch data from elasticsearch due to ConnectionError")
        value_list = ValueManagerES._ValueManagerES__create_value_objects(result["hits"]["hits"])
        if value_list:
           status = True
        else:
           status = False
        return status, value_list

    @staticmethod
    def delete(is_item, meta_dict):
        """
        deletes documents from elasticsearch
        """
        from monitor.utils import normalize_url
        body = ValueManagerES._ValueManagerES__create_delete_body(is_item, meta_dict)
        url = normalize_url(ELASTICSEARCH_URL)
        if url.endswith("9200"):                    # normalise url
            url = url + "/" + ValueManagerES.index_name_prefix + "*/" + ValueManagerES.doc_type + "/_query"
        elif url.endswith("9200/"):                 # normalise url
            url = url + ValueManagerES.index_name_prefix + "*/" + ValueManagerES.doc_type + "/_query"
        else:                                       # normalise url
             url = url + "9200/" + ValueManagerES.index_name_prefix + "*/" + ValueManagerES.doc_type + "/_query"
        print url
        body = json.dumps(body)
        headers = {'content-type': 'application/json', 'cache-control': 'no-cache'}
        result = requests.delete(url, headers = headers, data = body)
        if result.status_code == 200:
            return True, result
        else:
            return False, result
