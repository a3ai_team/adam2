# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-03 06:20
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('monitor', '0006_auto_20160803_0111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='scenario',
            name='Item',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='scenario', to='monitor.Item'),
        ),
    ]
