from __future__ import unicode_literals

from django.db import models
from inventory.models import Host, Application, Service
from project.models import Incident, Project
from notifications.models import Trigger
from django.db.models.signals import post_save, pre_delete
from django.core.exceptions import ObjectDoesNotExist
from celery.decorators import task
from notifications.models import Trigger
from monitor.utils import should_create_incident
from notifications.tasks import process_trigger
from django_extensions.db.fields import UUIDField
from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.sites.models import Site
from .manager import ValueManagerES, value_post_save
import logging
import json
import uuid

logger = logging.getLogger(__name__)


class API_check(models.Model):
    """
    Denotes an api that is to be monitored
    """
    api_name = models.CharField(max_length=255, null=False, blank=False)
    api_url = models.URLField(max_length=400, null=False, blank=False)
    api_url_secure = models.BooleanField(default=True)
    REQUEST_TYPES = (
        ('GET', 'GET'),
        ('POST', 'POST'),
    )
    request_type = models.CharField(max_length=255, null=False, blank=False, choices=REQUEST_TYPES, default='GET')
    request_payload = models.TextField(null=True, blank=True)
    request_headers = models.TextField(null=True, blank=True)
    project = models.ForeignKey(Project, null=True, blank=True, related_name='api_checks', on_delete=models.CASCADE)
    ENV_TYPES = (
        ('prod', 'prod'),
        ('uat', 'uat'),
        ('qa', 'qa'),
        ('dev', 'dev'),
    )
    env = models.CharField(max_length=255, null=False, blank=False, choices=ENV_TYPES, default='prod')
    incidents = models.ManyToManyField(Incident, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return "%s" % (self.api_name)

def delete_values_of_deleted_api(sender, instance, **kwargs):
    """
    delete values from elasticsearch associated with the deleted api
    """
    if instance.api_check_items.exists():
        related_item_list = instance.api_check_items.values()
        is_item = False         # to create different body for item
        for item in related_item_list:      # if single api is configured in multiple items
            instance_meta = { "model_name" : "API_check", "model_id" : instance.id, "item" : item["id"] }
            status, result = ValueManagerES.delete(is_item, instance_meta)
            if status:
                json_result = json.loads(result.text)
                logger.debug("%d values deleted for API check with id %d and name %s" %(json_result["_indices"]["_all"]["deleted"], instance.id, instance.api_name))
            else:
                logger.debug("Error : - %s" %(result.text))

pre_delete.connect(delete_values_of_deleted_api, sender=API_check)

class Cron_check(models.Model):
    """
    Denotes a cron task that is to be monitored
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    guid = UUIDField(default=uuid.uuid4, primary_key=True)
    cron_defination = models.CharField(max_length=255, null=False, blank=False)
    cronhook_target_url = models.CharField(max_length=255, null=False, blank=False) #to be auto generated
    bracktracking_depth = models.IntegerField(null=True, blank=True, default=10)
    project = models.ForeignKey(Project, null=True, blank=True, related_name='cron_checks', on_delete=models.CASCADE)
    incidents = models.ManyToManyField(Incident, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return "%s" % (self.name)

def generate_cron_hook_url(sender, instance, created, **kwargs):
    """
    generate the cron hook url
    """
    if created:
        site = Site.objects.all()[0]
        url = site.domain
        instance.cronhook_target_url = url+'/update/cron/'+str(instance.guid)
        instance.save()

post_save.connect(generate_cron_hook_url, sender=Cron_check)

def update_cron_hook_url(sender, instance, created, **kwargs):
    """
    update all cron hook urls for regions when site url is updated
    """
    for cron_check in Cron_check.objects.all():
        site = Site.objects.all()[0]
        url = site.domain
        cron_check.cronhook_target_url = url+'/update/cron/'+str(cron_check.guid)
        cron_check.save()

post_save.connect(update_cron_hook_url, sender=Site)

class Item(models.Model):
    """
    Denotes a stat that is to be monitored
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    key = models.CharField(max_length=255, null=False, blank=False)
    TYPES = (
        ('Cloudwatch-CPU', 'Cloudwatch-CPU'),
        ('Cloudwatch-Memory', 'Cloudwatch-Memory'),
        ('Cloudwatch-Disk', 'Cloudwatch-Disk'),
        ('Application', 'Application'),
        ('API check', 'API check'),
        ('Service', 'Service'),
        ('Cron', 'Cron')
    )
    Type = models.CharField(max_length=255, null=False, blank=False, choices=TYPES, default='Host')
    refresh_interval_in_seconds = models.IntegerField(default=30)
    scenario_counter = models.IntegerField(null=True, blank=True)
    host = models.ManyToManyField(Host, blank=True, related_name='host_items')
    application = models.ManyToManyField(Application, blank=True, related_name='app_items')
    api_check = models.ForeignKey(API_check, null=True, blank=True, related_name='api_check_items', on_delete=models.SET_NULL)
    service = models.ManyToManyField(Service, blank=True, related_name='service_items')
    cron_check = models.OneToOneField(Cron_check, null=True, blank=True, related_name='cron_check_items', on_delete=models.CASCADE)
    project = models.ForeignKey(Project, null=True, blank=True, related_name='items', on_delete=models.CASCADE)

    PRIORITIES = (
        ('LOW', 'LOW'),
        ('MEDIUM', 'MEDIUM'),
        ('HIGH', 'HIGH')
    )
    priority = models.CharField(max_length=255, null=False, blank=False, choices=PRIORITIES, default='LOW')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)
    def __unicode__(self):
        return "%s" % (self.name)

def delete_values_of_deleted_item(sender, instance, **kwargs):
    """
    delete values from elasticsearch associated with the deleted item
    """
    is_item = True      # to create different body for item
    instance_meta = { "item" : instance.id }
    status, result = ValueManagerES.delete(is_item, instance_meta)
    if status:
        json_result = json.loads(result.text)
        logger.debug("%d values deleted for Item with id %d and name %s" %(json_result["_indices"]["_all"]["deleted"], instance.id, instance.name))
    else:
        logger.debug("Error : - %s" %(result.text))

pre_delete.connect(delete_values_of_deleted_item, sender=Item)


class Value(models.Model):
     """
     Denotes data fetched from a monitored item
     """
     Item = models.ForeignKey(Item, null=False, on_delete=models.CASCADE, related_name='values' )
     name = models.CharField(max_length=255, null=False, blank=False)
     http_response_code = models.IntegerField(null=True, blank=True)
     response_time = models.FloatField(null=True, blank=True)
     data = models.CharField(max_length=255, null=False, blank=False)
     model_instance = models.CharField(max_length=300, null=True, blank=True)
     derived_from = models.CharField(max_length=255, null=False, blank=False)
     created_at = models.DateTimeField(auto_now_add=True)
     updated_at = models.DateTimeField(auto_now=True)
     def __unicode__(self):
         return "%s" % (self.name)

class ValueModel():
    """
    Denotes data fetched from a monitored item
    """
    def __init__(self, item, name, http_response_code, response_time, data, model_name, model_id, derived_from, project, organisation, created_at, updated_at):
        self.Item = Item.objects.get(pk=item)
        self.name = name
        self.http_response_code = http_response_code
        self.response_time = response_time
        self.data = data
        self.model_name = model_name
        self.model_id =  model_id
        self.derived_from = derived_from
        self.project = project
        self.organisation = organisation
        self.created_at = created_at
        self.updated_at = updated_at

def create_incident(value, message):
    """
    create incident and associate it with the corresponding
    host/app/api-check, then call the corresponding trigger
    """
    instance_meta = { "model": value.model_name, "id": value.model_id }
    if instance_meta['model']== 'Host':
        host_id = instance_meta['id']
        instance = value.Item.host.get(id=host_id)
        project = instance.region.project
        meta_info = instance.aws_id
        Type = 'Host'
    elif instance_meta['model']== 'Application':
        app_id = instance_meta['id']
        instance = value.Item.application.get(id=app_id)
        project = instance.host.region.project
        meta_info = instance.fqdn
        Type = 'Application'
    elif instance_meta['model']== 'API_check':
        api_check_id = instance_meta['id']
        instance = value.Item.api_check
        project = instance.project
        meta_info = instance.api_url
        Type = 'API_check'
    else:
        logger.error("cannot determine which model the value with id {0} belongs to, incident creation aborted".format(value.id))

    item_name = value.Item.name
    item_id = value.Item.id
    item_dict = {"name":item_name, "id":item_id}
    priority = value.Item.priority
    if instance_meta['model']== 'API_check':
        entity_name = instance.api_name
    else:
        entity_name = instance.name
    # check if alert needs to be sent or not
    (create, latest_incident) = should_create_incident(value.Item, instance_meta['model'], instance.pk)
    if create:
        incident = Incident.objects.create(project=project, details=message, Type=Type, item=json.dumps(item_dict), priority=priority)
        instance.incidents.add(incident)
        logger.debug("incident created -- %s" % incident.pk)
        process_trigger.delay(entity_name, meta_info, value, project)
    else:
        """
        check if current_time - updated_at is >= silence_interval in associated trigger
        then send mail else, don't send mail
        """
        try:
            silence_interval = value.Item.scenario.trigger.silence_interval
        except ObjectDoesNotExist:
            silence_interval = settings.DEFAULT_SILENCE_INTERVAL
        native_updated_at = (latest_incident.updated_at + timedelta(hours=5, minutes=30)).replace(tzinfo=None)
        if datetime.now() - native_updated_at >= timedelta(minutes=silence_interval):
            logger.debug("silence internval elapsed..calling trigger")
            process_trigger.delay(entity_name, meta_info, value, project)
            latest_incident.save()
        else:
            logger.debug("silence internval not elapsed..ignoring trigger")


@task(name= "scenario_check")
def validate_scenario(value):
    """
    validates a value against a scenario
    """
    logger.debug("validating scenario for %s" % value)
    try:
        scenario = value.Item.scenario
    except ObjectDoesNotExist:
        logger.info("no scenario configured for {0}".format(value.Item))
        return
    if scenario.condition == '>':
        logger.debug("entering > scenario")
        if float(value.data) > float(scenario.value_to_be_compared_with):
            message = "Alert! {0} > {1}".format(value.data, scenario.value_to_be_compared_with)
            logger.info("scenario check positive, creating incident")
            create_incident(value, message)
        else:
            logger.debug("scenario validation complete, nothing to worry about")

    elif scenario.condition == '>=':
        logger.debug("entering >= scenario")
        if float(value.data) >= float(scenario.value_to_be_compared_with):
            message = "Alert! {0} >= {1}".format(value.data, scenario.value_to_be_compared_with)
            logger.debug("scenario check positive, creating incident")
            create_incident(value, message)
        else:
            logger.debug("scenario validation complete, nothing to worry about")

    elif scenario.condition == '<=':
        logger.debug("entering <= scenario")
        if float(value.data) <= float(scenario.value_to_be_compared_with):
            message = "Alert! {0} <= {1}".format(value.data, scenario.value_to_be_compared_with)
            logger.debug("scenario check positive, creating incident")
            create_incident(value, message)
        else:
            logger.debug("scenario validation complete, nothing to worry about")

    elif scenario.condition == '<':
        logger.debug("entering < scenario")
        if float(value.data) < float(scenario.value_to_be_compared_with):
            message = "Alert! {0} < {1}".format(value.data, scenario.value_to_be_compared_with)
            logger.debug("scenario check positive, creating incident")
            create_incident(value, message)
        else:
            logger.debug("scenario validation complete, nothing to worry about")

    elif scenario.condition == '=':
        logger.debug("entering = scenario")
        if str(value.data) == str(scenario.value_to_be_compared_with):
            message = "Alert! {0} = {1}".format(value.data, scenario.value_to_be_compared_with)
            logger.debug("scenario check positive, creating incident")
            create_incident(value, message)
        else:
            logger.debug("scenario validation complete, nothing to worry about")

    elif scenario.condition == '!=':
        logger.debug("entering != scenario")
        if str(value.data) != str(scenario.value_to_be_compared_with):
            message = "Alert! {0} != {1}".format(value.data, scenario.value_to_be_compared_with)
            logger.debug("scenario check positive, creating incident")
            create_incident(value, message)
        else:
            logger.debug("scenario validation complete, nothing to worry about")


def validate_value(sender, instance, created, **kwargs):
    """
    performs a check of scenrios on the value obtained
    """
    if created: #this makes sure value object is validated only when its created and not everytime its saved
        if instance.http_response_code == 200:
            validate_scenario.delay(instance) # runs in async mode
        else:
            """
            Non, 200 code received create an incident and trigger the alert configured for the api-check
            """
            message = "Alert !received non 200 http code, code: {0}".format(instance.http_response_code)
            create_incident(instance, message)

# hook into the post save signal emitted by the value instance
value_post_save.connect(validate_value, sender=ValueManagerES)

class Scenario(models.Model):
    """
    Denotes the condition on which incidents are to be triggered
    """
    name = models.CharField(max_length=255, null=True, blank=False)
    Item = models.OneToOneField(Item, null=False, on_delete=models.CASCADE, related_name='scenario')
    CONDITIONS = (
    ('>', '>'),
    ('<', '<'),
    ('=', '='),
    ('!=', '!='),
    ('>=', '>='),
    ('<=', '<='),
    )
    trigger = models.ForeignKey(Trigger, null=True, related_name='scenarios', on_delete=models.SET_NULL)
    condition = models.CharField(max_length=255, null=False, blank=False, choices=CONDITIONS, default='=')
    value_to_be_compared_with = models.CharField(max_length=255, null=False, blank=False)
    number_of_subsequent_tries_after_which_scenario_triggers = models.IntegerField(default=1)
