from django.shortcuts import render
from monitor.models import Item, Value
from monitor.utils import (normalize_url, make_request, fetch_cloudwatch_metric, should_create_incident)
import logging
import requests
import json
from project.models import Incident
from celery.task.schedules import crontab
from celery.decorators import periodic_task, task
from notifications.utils import send_mail_via_ses
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.sites.models import Site
import json
from .manager import ValueManagerES

logger = logging.getLogger(__name__)

def get_footer(incident_guid):
    """
    create footer containing ack url and resolution url
    """
    site = Site.objects.all()[0]
    website_url = site.domain
    footer_salutation = "\n\nYours Faithfully\nSleuth"+"\n"+website_url+"/admin"
    ack_url = website_url+'/update/incident/'+str(incident_guid)+'/?action=Acknowledged'
    resolve_url = website_url+'/update/incident/'+str(incident_guid)+'/?action=Resolved'
    footer = '\n\nTo acknowledge the incident, click the link below:\n'+ack_url+'\n\nTo resolve the incident, click the link below:\n'+resolve_url+footer_salutation
    return footer

@periodic_task(run_every=crontab())
def monitor_items():
    """
    fetch items that need to be monitored them
    """
    logger.debug("invoking item monitoring task")
    # fetch all active items
    active_items = Item.objects.all().filter(is_active=True)
    for item in active_items:
        process_item.delay(item) # runs in async mode

@task(name = "process_item")
def process_item(item):
    """
    routes the item to check performing fucntion based on item type
    """
    try:
        silence_interval = item.scenario.trigger.silence_interval
    except ObjectDoesNotExist:
        silence_interval = settings.DEFAULT_SILENCE_INTERVAL

    if item.Type == "Cloudwatch-CPU":
        monitor_host(item, 'CPUUtilization', silence_interval) # pass control to host monitoring function to fetch CPU metrics from cloudwatch
    elif item.Type == "Cloudwatch-Memory":
        monitor_host(item, 'MEMORYUtilization', silence_interval) # pass control to host monitoring function to fetch Memory metrics from cloudwatch
    elif item.Type == "Cloudwatch-Disk":
        monitor_host(item, 'DISKUtilization', silence_interval) # pass control to host monitoring function to fetch Disk metrics from cloudwatch
    elif item.Type == "API check":
        monitor_api(item, silence_interval) # pass control to api monitoring function
    elif item.Type == "Application":
        monitor_app(item, silence_interval) # pass control to application monitoring function
    else:
        logger.error("item type unknown for %s" % item) # miscounfigured item found, log an error and do nothing

def monitor_host(item, metric, silence_interval):
    """
    monitors the url related with the host for which the item is configured
    """
    for host in item.host.filter(state='running'):
        (status, metric_value) = fetch_cloudwatch_metric(host.aws_id, metric)
        subject = "Alert! for host {0}".format(host.name)
        if status and metric_value:
            instance_meta = json.dumps({"id":host.id, "model":"Host"})
            value = ValueManagerES.create_document(item.pk, item.name, host.name, metric_value['value'], metric_value['timestamp'], 200, str(instance_meta), item.project_id, item.project.organisation.id)
            logger.debug("new value %s created post successfull monitoring of item %s" %(value, item))
        elif status and (metric_value is None):
            details = ("No cloudwatch data {0} found for instance {1}".format(metric, host.aws_id))
            logger.debug(details)

            #create an incident and send mail incase incident is not created already
            (create, latest_incident) = should_create_incident(item, 'Host', host.pk)
            if create:
                incident = Incident.objects.create(project=host.region.project, details=details, Type='HOST', item=json.dumps({"name":item.name, "id":item.id}), priority='HIGH')
                host.incidents.add(incident)
                footer = get_footer(incident.guid)
                send_mail_via_ses.delay(host.region.project, subject, details+footer)
            else:
                """
                check if current_time - updated_at is >= silence_interval in associated trigger
                then send mail else, don't send mail
                """
                native_updated_at = (latest_incident.updated_at + timedelta(hours=5, minutes=30)).replace(tzinfo=None)
                if datetime.now() - native_updated_at >= timedelta(minutes=silence_interval):
                    logger.debug("silence internval elapsed..sending mail")
                    footer=get_footer(latest_incident.guid)
                    send_mail_via_ses.delay(host.region.project, subject, details+footer)
                    latest_incident.save()
                else:
                    logger.debug("silence internval not elapsed..not sending mail")


        elif not status:
            details = "something went wrong while fetching metric {0} from cloudwatch for {1}".format(metric, host.aws_id)
            logger.debug(details)

            # create incident and send mail
            (create, latest_incident) = should_create_incident(item, 'Host', host.pk)
            if create:
                incident = Incident.objects.create(project=host.region.project, details=details, Type='HOST', item=json.dumps({"name":item.name, "id":item.id}), priority='HIGH')
                host.incidents.add(incident)
                footer = get_footer(incident.guid)
                send_mail_via_ses.delay(host.region.project, subject, details+footer)
            else:
                """
                check if current_time - updated_at is >= silence_interval in associated trigger
                then send mail else, don't send mail
                """
                native_updated_at = (latest_incident.updated_at + timedelta(hours=5, minutes=30)).replace(tzinfo=None)
                if datetime.now() - native_updated_at >= timedelta(minutes=silence_interval):
                    logger.debug("silence internval elapsed..sending mail")
                    send_mail_via_ses.delay(host.region.project, subject, details+footer)
                    latest_incident.save()
                else:
                    logger.debug("silence internval not elapsed..not sending mail")

def monitor_app(item, silence_interval):
    """
    monitors the heartbeat url related with the app for which the item is configured
    """
    key_to_be_looked_up_in_heartbeat_response = item.key
    for app in item.application.all():
        if app.host.state.lower() == 'running':
            heartbeat_url = normalize_url(app.monitoring_url, app.monitoring_url_secure)
            subject = "Alert! for heartbeat url {0}".format(heartbeat_url)
            # send a get request to the api
            response = make_request(heartbeat_url, 'GET')
            if response is not None:
                status_code = response.status_code
                if status_code != 200:
                    details = "heartbeat url {0} returned non 200 http response code, code: {1}".format(heartbeat_url, status_code)
                    logger.debug(details)

                    # create an incident and send mail
                    (create, latest_incident) = should_create_incident(item, 'Application', app.pk)
                    if create:
                        incident = Incident.objects.create(project=app.host.region.project, details=details, Type='APPLICATION', item=json.dumps({"name":item.name, "id":item.id}), priority='HIGH')
                        app.incidents.add(incident)
                        footer = get_footer(incident.guid)
                        send_mail_via_ses.delay(app.host.region.project, subject, details+footer)
                    else:
                        """
                        check if current_time - updated_at is >= silence_interval in associated trigger
                        then send mail else, don't send mail
                        """
                        native_updated_at = (latest_incident.updated_at + timedelta(hours=5, minutes=30)).replace(tzinfo=None)
                        if datetime.now() - native_updated_at >= timedelta(minutes=silence_interval):
                            logger.debug("silence internval elapsed..sending mail")
                            footer=get_footer(latest_incident.guid)
                            send_mail_via_ses.delay(app.host.region.project, subject, details+footer)
                            latest_incident.save()
                        else:
                            logger.debug("silence internval not elapsed..not sending mail")
                try:
                    data = json.loads(response.text)[key_to_be_looked_up_in_heartbeat_response]
                except Exception as e:
                    logger.error("malformed response or key '%s' not found in response for item:'%s'"%(key_to_be_looked_up_in_heartbeat_response, item))
                else:
                    instance_meta = json.dumps({"id":app.id, "model":"Application"})
                    value = ValueManagerES.create_document(item.pk, item.name, app.name, data, datetime.utcnow() , status_code, str(instance_meta), item.project_id, item.project.organisation.id, response.elapsed.total_seconds())
                    logger.debug("new value %s created post successfull monitoring of item %s " %(value, item))

            else:
                """
                api call failed, create an incident and trigger the alert configured for
                the api-check
                """
                details = "heartbeat url {0} not reachable".format(heartbeat_url)
                logger.debug(details)

                # create an incident and send mail
                (create, latest_incident) = should_create_incident(item, 'Application', app.pk)
                if create:
                    incident = Incident.objects.create(project=app.host.region.project, details=details, Type='APPLICATION', item=json.dumps({"name":item.name, "id":item.id}), priority='HIGH')
                    app.incidents.add(incident)
                    footer = get_footer(incident.guid)
                    send_mail_via_ses.delay(app.host.region.project, subject, details+footer)
                else:
                    """
                    check if current_time - updated_at is >= silence_interval in associated trigger
                    then send mail else, don't send mail
                    """
                    native_updated_at = (latest_incident.updated_at + timedelta(hours=5, minutes=30)).replace(tzinfo=None)
                    if datetime.now() - native_updated_at >= timedelta(minutes=silence_interval):
                        logger.debug("silence internval elapsed..sending mail")
                        footer=get_footer(latest_incident.guid)
                        send_mail_via_ses.delay(app.host.region.project, subject, details+footer)
                        latest_incident.save()
                    else:
                        logger.debug("silence internval not elapsed..not sending mail")

def monitor_api(item, silence_interval):
    """
    Monitors the api url which is mentioned in the item and generates a value
    support only get and post methods for now, expects a json response
    """
    try:
        api_to_be_monitored = item.api_check
    except ObjectDoesNotExist:
        logger.debug("No api associated with item, exiting")
        return
    key_to_be_looked_up_in_api_response = item.key
    api_url = normalize_url(api_to_be_monitored.api_url, api_to_be_monitored.api_url_secure)
    logger.debug(api_url)
    subject = "Alert! for api url {0}".format(api_url)
    request_type = api_to_be_monitored.request_type
    header = api_to_be_monitored.request_headers or None
    payload = api_to_be_monitored.request_payload or None
    response = None
    if (header is None and payload is None):
        logger.debug("no header and payload supplied")
        response = make_request(api_url, request_type)
    else:
        response = make_request(api_url, request_type, payload, header)

    if response is not None:
        try:
            status_code = response.status_code or None
            data = json.loads(response.text)[key_to_be_looked_up_in_api_response]
        except Exception as e:
            details = "malformed response or key '%s' not found in response for item:'%s' in api %s"%(key_to_be_looked_up_in_api_response, item, api_to_be_monitored)
            logger.error(details)

            #create an incident and send mail
            (create, latest_incident) = should_create_incident(item, 'API_check')
            if create:
                incident = Incident.objects.create(project=api_to_be_monitored.project, details=details, Type='API_check', item=json.dumps({"name":item.name, "id":item.id}), priority='HIGH')
                api_to_be_monitored.incidents.add(incident)
                footer = get_footer(incident.guid)
                send_mail_via_ses.delay(api_to_be_monitored.project, subject, details+footer)
            else:
                """
                check if current_time - updated_at is >= silence_interval in associated trigger
                then send mail else, don't send mail
                """
                native_updated_at = (latest_incident.updated_at + timedelta(hours=5, minutes=30)).replace(tzinfo=None)
                if datetime.now() - native_updated_at >= timedelta(minutes=silence_interval):
                    logger.debug("silence internval elapsed..sending mail")
                    footer=get_footer(latest_incident.guid)
                    send_mail_via_ses.delay(api_to_be_monitored.project, subject, details+footer)
                    latest_incident.save()
                else:
                    logger.debug("silence internval not elapsed..not sending mail")
        else:
            instance_meta = json.dumps({"id":api_to_be_monitored.id, "model":"API_check"})
            value = ValueManagerES.create_document(item.pk, item.name, api_to_be_monitored, data, datetime.utcnow() , status_code, str(instance_meta), item.project_id, item.project.organisation.id, response.elapsed.total_seconds())
            logger.debug("new value %s created post successfull monitoring of item %s " %(value, item))

    else:
        """
        api call failed, create an incident and trigger the alert configured for
        the api-check
        """
        details = "api-check url {0} not reachable".format(api_url)
        logger.debug(details)

        #create an incident and send mail
        (create, latest_incident) = should_create_incident(item, 'API_check')
        if create:
            incident = Incident.objects.create(project=api_to_be_monitored.project, details=details, Type='API_check', item=json.dumps({"name":item.name, "id":item.id}), priority='HIGH')
            api_to_be_monitored.incidents.add(incident)
            footer = get_footer(incident.guid)
            send_mail_via_ses.delay(api_to_be_monitored.project, subject, details+footer)
        else:
            """
            check if current_time - updated_at is >= silence_interval in associated trigger
            then send mail else, don't send mail
            """
            native_updated_at = (latest_incident.updated_at + timedelta(hours=5, minutes=30)).replace(tzinfo=None)
            if datetime.now() - native_updated_at >= timedelta(minutes=silence_interval):
                logger.debug("silence internval elapsed..sending mail")
                footer=get_footer(latest_incident.guid)
                send_mail_via_ses.delay(api_to_be_monitored.project, subject, details+footer)
                latest_incident.save()
            else:
                logger.debug("silence internval not elapsed..not sending mail")
