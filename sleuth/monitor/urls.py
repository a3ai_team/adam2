try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
    from django.conf.urls.defaults import *

from .views import apichecks_list,apichecks_create,apichecks_view,apichecks_edit,apichecks_delete, scenarios_list, scenarios_delete, scenarios_create, scenarios_view, scenarios_edit,cron_list,cron_delete,cron_create,cron_edit,cron_view,item_list,item_delete,item_create,item_view,item_edit

urlpatterns=[
    url(r'^apichecks/list/(?P<org>[\w-]+)/(?P<project>[\w-]+)$',apichecks_list,name='apichecks_list'),
    url(r'^apichecks/list/all/$',apichecks_list,name='apichecks_list'),
    url(r'^apichecks/create/(?P<org>[\w-]+)/(?P<project>[\w-]+)$',apichecks_create,name='apichecks_create'),
    url(r'^apichecks/create/$',apichecks_create,name='apichecks_create'),
    url(r'^apichecks/details/(?P<org>[\w-]+)/(?P<project>[\w-]+)/(?P<apichecks>[\w-]+)$',apichecks_view,name='apichecks_view'),
    url(r'^apichecks/edit/(?P<org>[\w-]+)/(?P<project>[\w-]+)/(?P<apichecks>[\w-]+)$',apichecks_edit,name='apichecks_edit'),
    url(r'^apichecks/delete/$', apichecks_delete, name='apichecks_delete'),
    url(r'^cron/list/$', cron_list, name='cron_list'),
    url(r'^cron/delete/$', cron_delete, name='cron_delete'),
    url(r'^cron/create/$', cron_create, name='cron_create'),
    url(r'^cron/view/(?P<id>[\w-]+)$', cron_view, name='cron_view'),
    url(r'^cron/edit/(?P<id>[\w-]+)$', cron_edit, name='cron_edit'),
    url(r'^scenarios/list/(?P<org>[\w-]+)/(?P<project>[\w-]+)$', scenarios_list, name='scenarios_list'),
    url(r'^scenarios/list/all/$',scenarios_list,name='scenarios_list'),
    url(r'^scenarios/create/(?P<org>[\w-]+)/(?P<project>[\w-]+)$', scenarios_create, name='scenarios_create'),
    url(r'^scenarios/create/$', scenarios_create, name='scenarios_create'),
    url(r'^scenarios/details/(?P<org>[\w-]+)/(?P<project>[\w-]+)/(?P<scenario>[\w-]+)$', scenarios_view, name='scenarios_view'),
    url(r'^scenarios/edit/(?P<org>[\w-]+)/(?P<project>[\w-]+)/(?P<scenario>[\w-]+)$', scenarios_edit, name='scenarios_edit'),
    url(r'^scenarios/delete/$', scenarios_delete, name='scenarios_delete'),
    url(r'^item/list/$', item_list, name="item_list"),
    url(r'^item/delete/$', item_delete, name="item_delete"),
    url(r'^item/create/$', item_create, name='item_create'),
    url(r'^item/view/(?P<id>[\w-]+)$', item_view, name='item_view'),
    url(r'^item/edit/(?P<id>[\w-]+)$', item_edit, name='item_edit'),
]
