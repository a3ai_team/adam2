#utilities for monitoring module
from eventlet import Timeout
import logging
import requests
from inventory.models import Host
from datetime import datetime, timedelta
from operator import itemgetter
from boto3.session import Session
from aliyunsdkcore import client
from aliyunsdkcms.request.v20160318 import QueryMetricListRequest
import json
import ast


logger = logging.getLogger(__name__)

def normalize_url(url, secure=None):
    """
    return a url in the format http://xyx.com or http://xx.xx.xx.xx
    """
    http_prefix = 'http://'
    if secure:
        http_prefix = 'https://'
        if url.startswith('http://'):
            return http_prefix + url[len('http://'):]
    if url.startswith('https://'):
        return http_prefix + url[len('https://'):]
    if url.startswith('http://www.'):
        return http_prefix + url[len('http://www.'):]
    if url.startswith('https://www.'):
        return http_prefix + url[len('https://www.'):]
    if url.startswith('www.'):
        return http_prefix + url[len('www.'):]
    if not (url.startswith('http://') or url.startswith('https://')):
        return http_prefix + url
    return url

def make_request(resp_url, request_type, payload=None, headers=None):
    """
    Make a http request and retry 4 times in case of timeout
    """
    if payload is not None:
        try:
            payload=json.dumps(payload)
            payload = json.loads(payload)
            payload=ast.literal_eval(payload)
        except ValueError:
            logger.debug("payload is not a valid json, ignoring payload")
            payload = None
    if headers is not None:
        try:
            headers=json.dumps(headers)
            headers = json.loads(headers)
            headers=ast.literal_eval(headers)
        except ValueError:
            logger.debug("headers is not a valid json, ignoring hdeaers")
            headers = None
    with Timeout(3, False):
        try:
            logger.debug('request type is {0}'.format(request_type))
            if request_type == 'GET':
                resp = requests.get(resp_url, verify=False, headers=headers, params=payload)
            elif request_type == 'POST':
                resp = requests.post(resp_url, verify=False, headers=headers, data=payload)
            else:
                logger.debug("unknown request type supplied while making http request, aborting..")
        except Exception as e:
            logger.debug('exception occured while making %s request, %s' % (request_type, e))
            return None

        else:
            if resp.status_code == requests.codes.ok:
                return resp

            else:
                logger.debug('recieved non OK Http status code -- {0}'.format(resp.status_code))
                return resp


def get_cloud_watch_session(instance_id):
    """
    returns a session that is to be used for fetching data from cloudwatch
    """

    try:
    # initialise the access key and region based on project and instance
        host = Host.objects.get(aws_id=instance_id)
        access_key_id = host.region.project.aws_access_key_id
        secret_access_key = host.region.project.aws_secret_access_key
        region = host.region.region_code

        # create a boto session object
        session = Session(
            aws_access_key_id=access_key_id,
            aws_secret_access_key=secret_access_key,
            region_name=region)
        return True, session # return session object and true

    except Exception:
        return False, None


def fetch_cloudwatch_metric(instance_id, metric_name):
    """
    Fetches the latest data for given metric from cloudwatch for an instance
    """
    # create a instance specific sesssion
    (status, session) = get_cloud_watch_session(instance_id)
    if not status:
        logger.debug("not able to obtain create session object..exiting")
        return (False, None)
    cw = session.client('cloudwatch')
    namespace = 'System/Linux'
    now = datetime.utcnow()
    past = now - timedelta(minutes=30)
    future = now + timedelta(minutes=10)
    if metric_name == 'CPUUtilization': namespace='AWS/EC2'
    # call cloudwatch api to get metric of the given instance
    logger.debug("fetching data for instance_id:  {0}".format(instance_id))
    resp = cw.get_metric_statistics(
        Namespace=namespace,
        MetricName= metric_name, # eg. CPUUtilization
        Dimensions=[{'Name': 'InstanceId', 'Value': instance_id}],
        StartTime=past,
        EndTime=future,
        Period=300,
        Statistics=['Average'])

    datapoints = resp['Datapoints']
    if len(datapoints) == 0:
        logger.debug("No data obtained for {0}".format(instance_id))
        return (True, None)
    last_datapoint = sorted(datapoints, key=itemgetter('Timestamp'))[-1]
    utilization = last_datapoint['Average']
    timestamp = last_datapoint['Timestamp']+timedelta(hours=5, minutes=30)
    if metric_name == 'CPUUtilization':
        load = round(utilization, 2)
        return (True, {"value":load, "timestamp": timestamp})
    elif metric_name == 'MEMORYUtilization':
        return (True, {"value":round(utilization, 2), "timestamp": timestamp})
    elif metric_name == 'DISKUtilization':
        return (True, {"value":round(utilization, 2), "timestamp": timestamp})
    else:
        logger.debug("unsupported metric")
        return (False, "unsupported metric")

def get_cloud_monitor_session_alibaba(instance_id):
    """
    returns a session that is to be used for fetching data from cloudmonitor alibaba
    """

    try:
    # initialise the access key and region based on project and instance
        #host = Host.objects.get(aws_id=instance_id)
        access_key_id = "dsxf366wUNg3xppj"
        secret_access_key = "hBJEWi3uUmXcLI7vNqeoakgZtEgCPe"
        region = "ap-southeast-1"

        # create a  session object
        session=client.AcsClient(access_key_id,secret_access_key,region)
        return True, session # return session object and true

    except Exception:
        return False, None



def fetch_cloudmonitor_metric_alibaba(instance_id, metric_name):
    """
    Fetches the latest data for given metric from cloudmonitor alibaba for an instance
    """
    # create a instance specific sesssion
    instance_id="i-22sxi9i4a"
    (status, session) = get_cloud_monitor_session_alibaba(instance_id)
    if not status:
        logger.debug("not able to obtain create session object..exiting")
        return (False, None)
    #cw = session.client('cloudmonitor alibaba')
    namespace = 'acs_ecs'
    now = datetime.utcnow()
    past = now - timedelta(minutes=30)
    future = now + timedelta(minutes=10)
    # call cloudmonitor alibaba api to get metric of the given instance
    logger.debug("fetching data for instance_id:  {0}".format(instance_id))
    request_data = QueryMetricListRequest.QueryMetricListRequest()
    request_data.set_accept_format('json')
    request_data.set_Project(namespace)
    request_data.set_Metric(metric_name)
    request_data.set_Dimensions(str({'instanceId':instance_id}))
    request_data.set_StartTime(past)
    request_data.set_EndTime(future)
    request_data.set_Period('300')
    result=session.do_action(request_data)
    json_converted_data = result.replace("'", "\"")
    resp=json.loads(json_converted_data)
    datapoints = resp['Datapoints']
    if len(datapoints) == 0:
        logger.debug("No data obtained for {0}".format(instance_id))
        return (True, None)
    last_datapoint = sorted(datapoints, key=itemgetter('timestamp'))[-1]
    utilization = last_datapoint['Average']

    timestamp = datetime.fromtimestamp(last_datapoint['timestamp']/1e3)+timedelta(hours=5, minutes=30)
    if metric_name == 'CPUUtilization':
        load = round(utilization, 2)
        return (True, {"value":load, "timestamp": timestamp})
    elif metric_name == 'vm.MemoryUtilization':
        return (True, {"value":round(utilization, 2), "timestamp": timestamp})
    elif metric_name == 'vm.DiskUtilization':
        return (True, {"value":round(utilization, 2), "timestamp": timestamp})
    else:
        logger.debug("unsupported metric")
        return (False, "unsupported metric")

def should_create_incident(item, instance_type, ID=None):
    """
    check wheather or not incident should be created based on pre-existing incidents for a given resource
    return True or False, incase false also returns the latest incident else None
    """
    from project.models import Incident
    Item = item
    if instance_type == 'Host':
        instance = item.host.get(pk=ID)
    elif instance_type == 'Application':
        instance = item.application.get(pk=ID)
    elif instance_type == 'Service':
        instance = item.Service.get(pk=ID)
    elif instance_type == 'API_check':
        instance = item.api_check
    elif instance_type == 'Cron_check':
        instance - item.cron_check
    else:
        logger.error("unknown instance type received while determining if incident should be created..aborting")
        return
    incidents = instance.incidents.filter(state__in=[Incident.TRIGGERED, Incident.ACKNOWLEDGED], Type=instance_type).order_by('-created_at')
    if len(incidents) == 0:
        return True, None
    else:
        incidents_corresponding_to_current_item = []
        for incident in incidents:
            incident_item = json.loads(incident.item)
            incident_item_id = incident_item['id']
            if incident_item_id == Item.pk:
                incidents_corresponding_to_current_item.append(incident)
        if len(incidents_corresponding_to_current_item) == 0:
            return True, None
        else:
            latest_incident = incidents_corresponding_to_current_item[0]
            return False, latest_incident
