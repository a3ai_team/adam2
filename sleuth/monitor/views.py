from .models import Cron_check, Value
from project.models import Project
from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
import json
from .models import API_check, Scenario, Item, Trigger,Item
from .forms import ApiCheckForm, ScenarioForm,ItemForm
from project.models import Project
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.http import Http404
from organisation.models import Organisation
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render,redirect,get_object_or_404
from .forms import CronCheckForm
from inventory.models import Host,Application,Service

def update_cron(request, cron_check_guid):
    """
    logs a ping coming from a cron job and creates an value object which can be tallied
    for monitoring if the cron ran properly
    """
    if request.method == 'GET' and (cron_check_guid is not None):
        #request.GET.get('key', '')
        crons = Cron_check.objects.filter(guid=cron_check_guid)
        if len(crons)==0:
            data = {"code":"404", "message":"resouce not found"}
            return JsonResponse(data, status=404)
        else:
            cron = crons[0]
            try:
                item = cron.cron_check_items
            except ObjectDoesNotExist:
                data = {"code":"200", "message":"No item has been configured for this cron job, aborting.."}
                return JsonResponse(data, status=200)
            else:
                instance_meta = json.dumps({"id":cron.pk, "model":"Cron_check"})
                Value.objects.create(Item=item, name=cron.name, derived_from=cron.name, data='1', http_response_code=200, model_instance=str(instance_meta))
                data = {"code":"201", "message":"created entry for cron check"}
                return JsonResponse(data, status=201)

@login_required
def apichecks_create(request,org=None,project=None):
    """
    create api_checks for specific project
    """
    form=ApiCheckForm(request.POST or None)

    if project==None and org==None:
        """
        if apichecks is created from sidebar
        """
        projects=Project.objects.filter(users=request.user.user)
        form.fields['project'].queryset=projects
        orgs=Organisation.objects.filter(users=request.user.user)
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('apichecks_list'))
    elif not project==None and not org==None:
        """
        if apichecks is created from going through project
        """
        try:
            project=Project.objects.get(users=request.user.user,pk=project)
        except:
            logger.debug("User trying to access others project or project is created yet")
            raise Http404("Invalid access to a project")
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('apichecks_list',args=[org,project.id]))
    else:
        logger.debug("trying to access wrong url")
        raise Http404("Invalid Page")
    return render(request,"monitor/apicheck_form.html",{'form':form ,'org':org,'project':project,'edit':0,'view':0})

@login_required
def apichecks_list(request,org=None,project=None):
    """
    Create apichecks for particular monitor in project
    """
    if project==None and org==None:
        """
        List all apichecks
        """
        apicheck_lists=API_check.objects.filter(project__users=request.user.user)

    elif not project==None and not org==None:
        """
        listing on the basis of project
        """
        try:
            project=Project.objects.get(pk=project,users=request.user.user)
            apicheck_lists=API_check.objects.filter(project__users=request.user.user,project=project)
        except:
            raise Http404("Invalid Page")
    else:
        raise Http404("Invalid Page")

    """
    pagination for apichecks
    """
    paginator=Paginator(apicheck_lists,7)
    page=request.GET.get('page')
    try:
        apichecks=paginator.page(page)
    except PageNotAnInteger:
        apichecks=paginator.page(1)
    except EmptyPage:
        apichecks=paginator.page(paginator.num_pages)
    return render(request,"monitor/apicheck_list.html",{'apichecks':apichecks,'project':project,'org':org})

@login_required
def apichecks_view(request,org,project,apichecks):
    """
    Read apichecks information
    """
    try:
        org_name=Organisation.objects.get(pk=org)
    except:
        raise Http404("Invalid Page")
    org_list=Organisation.objects.filter(users=request.user.user)
    if org_name in org_list:
        #check whether project is available or not
        project=get_object_or_404(Project,pk=project,users=request.user.user)
        api_check=get_object_or_404(API_check,pk=apichecks,project__users=request.user.user)
        form=ApiCheckForm(request.POST or None, instance=api_check)
    return render(request,"monitor/apicheck_form.html",{'form':form ,'org':org_name,'view':1,'edit':0,'project':project,'apichecks':api_check})

@login_required
def apichecks_edit(request,org,project,apichecks):
    """
    Edit apichecks information
    """
    try:
        org_name=Organisation.objects.get(pk=org)
    except:
        raise Http404("Invalid Page")
    org_list=Organisation.objects.filter(users=request.user.user)
    if org_name in org_list:
        #check whether project is available or not
        project=get_object_or_404(Project,pk=project,users=request.user.user)
        api_check=get_object_or_404(API_check,pk=apichecks,project__users=request.user.user)
        form=ApiCheckForm(request.POST or None, instance=api_check)
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('apichecks_list',args=[org,project.id]))
    return render(request,"monitor/apicheck_form.html",{'form':form ,'org':org_name,'view':0,'edit':1,'project':project,'apichecks':api_check})

@login_required
def apichecks_delete(request):
    """
    view for deleting project
    """
    org=request.POST.get('org')
    project=request.POST.get('project')
    if request.POST:
        try:
            list_delete=dict(request.POST)['apichecks_selected']
        except:
            list_delete=[]
        apichecks=API_check.objects.filter(pk__in=list_delete)
        if apichecks is not None:
            if (request.user.is_superuser) or (request.user.user in User.objects.filter()):
                apichecks.delete()
    if org and project:
        return redirect(reverse_lazy('apichecks_list' ,args=[org,project]))
    return redirect(reverse_lazy('apichecks_list'))

@login_required
def cron_list(request):
    """
    list all cron check created
    """
    kwargs = {}
    kwargs['project__users'] = request.user.user
    if request.GET.get('project'):
        kwargs['project'] = Project.objects.get(pk=int(request.GET.get('project')))
    crons = Cron_check.objects.filter(**kwargs)
    paginator = Paginator(crons,10)
    page = request.GET.get('page')
    try:
        crons = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        crons = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        crons = paginator.page(paginator.num_pages)
    projects = Project.objects.filter(users=request.user.user)
    return render(request,"monitor/cron_list.html",{'crons': crons,'projects':projects})

@login_required
def cron_delete(request):
    """
    view to delete multiple crons
    """
    if request.POST:
        try:
            list_delete=dict(request.POST)['cron_selected']
        except:
            list_delete=[]
        crons=Cron_check.objects.filter(pk__in=list_delete)
        if crons is not None:
            crons.delete()
    return redirect(reverse_lazy('cron_list'))

@login_required
def cron_create(request):
    """
    view for creating cron checks
    """
    form = CronCheckForm(request.POST or None)
    try:
        form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    except:
        #raise when url hit by user is not valid
        raise Http404(" Sorry Page is invalid ")
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('cron_list'))
    return render(request,"monitor/cron_check_form.html",{'form':form})

@login_required
def cron_view(request,id):
    """
    view for rendering cron information
    """
    cron = get_object_or_404(Cron_check,pk=id)
    form=CronCheckForm(request.POST or None, instance=cron)
    form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    return render(request,'monitor/cron_check_form.html',{'form':form,'view':1,'edit':0,'cron':cron})

@login_required
def cron_edit(request,id):
    """
    view to edit cron check instance
    """
    cron=get_object_or_404(Cron_check,pk=id)
    form=CronCheckForm(request.POST or None, instance=cron)
    form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('cron_list'))
    return render(request,"monitor/cron_check_form.html",{'form':form ,'view':0,'edit':1,'cron':cron})

@login_required
def scenarios_list(request, org=None, project=None):
    """
    list scenarios for a particular or all project based on logged in user
    """
    if not project == None and org == None:
        """
        list scenarios based on the project
        """
        try:
            project = Project.objects.get(pk=project)
            scenario_list = Scenario.objects.filter(Item__project=project, Item__project__users=request.user.user)
        except:
            raise Http404("Invalid Page")
    elif project == None and org == None:
        """
        list all scenarios
        """
        scenario_list = Scenario.objects.filter(Item__project__users=request.user.user)
        print scenario_list
    else:
        raise Http404("Invalid Page")
    page = request.GET.get('page')
    paginator = Paginator(scenario_list, 10)
    try:
        scenario_data = paginator.page(page)
    except PageNotAnInteger:
        scenario_data = paginator.page(1)
    except EmptyPage:
        scenario_data = paginator.page(paginator.num_pages)
    return render(request, "monitor/scenario_list.html", { "scenario_list" : scenario_data, "project" : project, "org" : org})

@login_required
def scenarios_delete(request):
    """
    view for deleting scenario
    """
    org = request.POST.get('org')
    project = request.POST.get('project')
    try:
        list_delete = dict(request.POST)['scenario_list']
    except:
        list_delete = []
    scenarios_list = Scenario.objects.filter(pk__in=list_delete,Item__project__users=request.user.user)
    if scenarios_list is not None:
        scenarios_list.delete()
    if not org == None and not project == None:
        return redirect(reverse_lazy('scenarios_list', args = [org, project]))
    return redirect(reverse_lazy('scenarios_list'))

@login_required
def scenarios_create(request, org=None, project=None):
    """
    create scenario for a particular project
    """
    form = ScenarioForm(request.POST or None)
    if project == None and org == None:
        project = Project.objects.filter(users=request.user.user)
        item = Item.objects.filter(project=project)
        trigger = Trigger.objects.filter(project=project)
        form.fields["Item"].queryset = item
        form.fields["trigger"].queryset = trigger
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('scenarios_list'))
    elif not project == None and org == None:
        try:
            project = Project.objects.filter(pk=project)
        except:
            logger.debug("User trying to access others project or project is created yet")
            raise Http404("Invalid access to a project")
        item = Item.objects.filter(project=project)
        trigger = Trigger.objects.filter(project=project)
        form.fields["Item"].queryset = item
        form.fields["trigger"].queryset = trigger
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('scenarios_list', args=[org,project.id]))
        else:
            logger.debug("trying to access wrong url")
            raise Http404("Invalid Page")
    return render(request, 'monitor/scenario_form.html', { "form": form, "project": project, "org": org, "edit": 0, "view": 0})

@login_required
def scenarios_view(request, org, project, scenario):
    """
    Read apichecks information
    """
    project_obj = get_object_or_404(Project, pk=project)
    scenario_obj = get_object_or_404(Scenario, pk=scenario, Item__project__users=request.user.user)
    form = ScenarioForm(request.POST or None, instance=scenario_obj)
    return render(request, 'monitor/scenario_form.html', { "form": form, "scenario": scenario_obj, "project": project_obj, "org": org, "edit": 0, "view": 1})

@login_required
def scenarios_edit(request, org, project, scenario):
    """
    Edit apichecks information
    """
    project_obj = get_object_or_404(Project, pk=project)
    scenario_obj = get_object_or_404(Scenario, pk=scenario, Item__project__users=request.user.user)
    form = ScenarioForm(request.POST or None, instance = scenario_obj)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('scenarios_list'))
    return render(request, 'monitor/scenario_form.html', { "form": form, "scenario": scenario_obj, "project": project_obj, "org": org, "edit": 1, "view": 0})

@login_required
def item_list(request):
    """
    view for list all items
    """
    kwargs = {}
    kwargs['project__users'] = request.user.user
    if request.GET.get('project'):
        kwargs['project'] = Project.objects.get(pk=int(request.GET.get('project')))
    items = Item.objects.filter(**kwargs)
    paginator = Paginator(items,10)
    page = request.GET.get('page')
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        items = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        items = paginator.page(paginator.num_pages)
    projects = Project.objects.filter(users=request.user.user)
    return render(request, "monitor/item_list.html",{'items':items,'projects':projects})

@login_required
def item_delete(request):
    """
    view to delete multiple items
    """
    if request.POST:
        try:
            list_delete=dict(request.POST)['item_selected']
        except:
            list_delete=[]
        items=Item.objects.filter(pk__in=list_delete)
        if items is not None:
            items.delete()
    return redirect(reverse_lazy('item_list'))

@login_required
def item_create(request):
    """
    view for creating item
    """
    form = ItemForm(request.POST or None)
    try:
        form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    except:
        #raise when url hit by user is not valid
        raise Http404(" Sorry Page is invalid ")
    if request.POST.get('project'):
        form.fields['cron_check'].queryset = Cron_check.objects.filter(project__id=int(request.POST.get('project')))
        form.fields['api_check'].queryset = API_check.objects.filter(project__id=int(request.POST.get('project')))
        form.fields['host'].queryset = Host.objects.filter(region__project__id=int(request.POST.get('project')))
        form.fields['application'].queryset = Application.objects.filter(host__region__project__id=int(request.POST.get('project')))
        form.fields['service'].queryset = Service.objects.filter(host__region__project__id=int(request.POST.get('project')))
    else:
        form.fields['cron_check'].queryset = Cron_check.objects.none()
        form.fields['api_check'].queryset = API_check.objects.none()
        form.fields['host'].queryset = Host.objects.none()
        form.fields['application'].queryset = Application.objects.none()
        form.fields['service'].queryset = Service.objects.none()
    if request.POST:
        if request.POST.get('Create'):
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('item_list'))
    return render(request,"monitor/item_form.html",{'form':form})

@login_required
def item_view(request,id):
    """
    view for rendering item information
    """
    item = get_object_or_404(Item,pk=id)
    form=ItemForm(request.POST or None, instance=item)
    form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    if item.project:
        form.fields['cron_check'].queryset = Cron_check.objects.filter(project__id=item.project.id)
        form.fields['api_check'].queryset = API_check.objects.filter(project__id=item.project.id)
        form.fields['host'].queryset = Host.objects.filter(region__project__id=item.project.id)
        form.fields['application'].queryset = Application.objects.filter(host__region__project__id=item.project.id)
        form.fields['service'].queryset = Service.objects.filter(host__region__project__id=item.project.id)
    else:
        form.fields['cron_check'].queryset = Cron_check.objects.none()
        form.fields['api_check'].queryset = API_check.objects.none()
        form.fields['host'].queryset = Host.objects.none()
        form.fields['application'].queryset = Application.objects.none()
        form.fields['service'].queryset = Service.objects.none()
    return render(request,'monitor/item_form.html',{'form':form,'view':1,'edit':0,'item':item})

@login_required
def item_edit(request,id):
    """
    view to edit item instance
    """
    item=get_object_or_404(Item,pk=id)
    form=ItemForm(request.POST or None, instance=item)
    form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    if item.project:
        form.fields['cron_check'].queryset = Cron_check.objects.filter(project__id=item.project.id)
        form.fields['api_check'].queryset = API_check.objects.filter(project__id=item.project.id)
        form.fields['host'].queryset = Host.objects.filter(region__project__id=item.project.id)
        form.fields['application'].queryset = Application.objects.filter(host__region__project__id=item.project.id)
        form.fields['service'].queryset = Service.objects.filter(host__region__project__id=item.project.id)
    else:
        form.fields['cron_check'].queryset = Cron_check.objects.none()
        form.fields['api_check'].queryset = API_check.objects.none()
        form.fields['host'].queryset = Host.objects.none()
        form.fields['application'].queryset = Application.objects.none()
        form.fields['service'].queryset = Service.objects.none()
    if request.POST:
        if request.POST.get('Update'):
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('item_list'))
    return render(request,"monitor/item_form.html",{'form':form ,'view':0,'edit':1,'item':item})
