# -*- coding: utf-8 -*-
from django.contrib import admin
from .filters import ActionFieldFilter
from .models import Action_profile, Trigger
from project.models import Project
from notifications.models import Action_profile, Trigger
from django.contrib import admin
from djcelery.models import (TaskState, WorkerState,
                 PeriodicTask, IntervalSchedule, CrontabSchedule)


class Action_profileAdmin(admin.ModelAdmin):
    list_display = (
        u'id',
        'name',
        'Trigger_pager_duty_event',
        'Trigger_sound_alarm',
        'Trigger_sms_alert',
        'Triger_email_alert',
        'created_at',
        'updated_at',
    )
    list_filter = (
        'Trigger_pager_duty_event',
        'Trigger_sound_alarm',
        'Trigger_sms_alert',
        'Triger_email_alert',
        'created_at',
        'updated_at',
    )
    search_fields = ('name',)
    date_hierarchy = 'created_at'

    def get_queryset(self, request):
        #filter project based on logged in user
        query = super(Action_profileAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(trigger__project__users=request.user.user).order_by('-created_at')

admin.site.register(Action_profile, Action_profileAdmin)


class TriggerAdmin(admin.ModelAdmin):
    list_display = (
        u'id',
        'name',
        'Action',
        'Type',
        'created_at',
        'updated_at',
    )

    list_filter = (ActionFieldFilter, 'created_at', 'updated_at')

    search_fields = ('name',)
    date_hierarchy = 'created_at'

    def get_queryset(self, request):
        #filter project based on logged in user
        query = super(TriggerAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(project__users=request.user.user).order_by('-created_at')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Action and project where users project belongs
        """
        if not request.user.is_superuser:
            if db_field.name == "Action" :
                kwargs["queryset"] = Action_profile.objects.filter(trigger__project__users=request.user.user)
            if db_field.name == "project" :
                kwargs["queryset"] = Project.objects.filter(users=request.user.user)
        return super(TriggerAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(Trigger, TriggerAdmin)

#unregister admin modules of djcelery from admin panel
admin.site.unregister(TaskState)
admin.site.unregister(WorkerState)
admin.site.unregister(IntervalSchedule)
admin.site.unregister(CrontabSchedule)
admin.site.unregister(PeriodicTask)
