from django.contrib import admin
from .models import Action_profile
from django.contrib.admin import SimpleListFilter

class ActionFieldFilter(SimpleListFilter):
    """
    Define Filters for Items in filter option
    """
    title = 'Action'
    parameter_name = 'Action'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            action=Action_profile.objects.filter()
        else:
            action = Action_profile.objects.filter(trigger__project__users=request.user.user)
        return [(a['id'],a['name']) for a in action.values()]

    def queryset(self, request,queryset):
        if self.value():
            return queryset.filter(Action_id=self.value())
        else:
            return queryset
