from django import forms
from .models import Action_profile,Trigger

# place form definition here
class ActionForm(forms.ModelForm):
    """
    Form for Action model
    """
    class Meta:
        model=Action_profile
        fields=['name','Trigger_pager_duty_event','Trigger_sound_alarm','Trigger_sms_alert','Triger_email_alert']

class TriggerForm(forms.ModelForm):
    """
    Form for Trigger model
    """
    class Meta:
        model=Trigger
        fields=['name','Action','Type','project','silence_interval']