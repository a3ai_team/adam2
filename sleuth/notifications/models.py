from __future__ import unicode_literals
from django.db import models
from project.models import Project

class Action_profile(models.Model):
    """
    Denotes a set of task configurations that can be fired in response to an event
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    Trigger_pager_duty_event = models.BooleanField(default=True)
    Trigger_sound_alarm = models.BooleanField(default=True)
    Trigger_sms_alert = models.BooleanField(default=True)
    Triger_email_alert = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s" % (self.name)

class Trigger(models.Model):
    """
    Event Trigger, denotes an event handling process
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    Action = models.ForeignKey(Action_profile, null=True, on_delete=models.SET_NULL)
    HOST = 'Host'
    APPLICATION = 'Application'
    API_CHECK = 'API_check'
    SERVICE = 'Service'
    CRON = 'Cron_check'
    TYPES = (
        (HOST, 'HOST'),
        (APPLICATION, 'APPLICATION'),
        (API_CHECK, 'API_check'),
        (SERVICE, 'SERVICE'),
        (CRON, 'CRON_check')
    )
    Type = models.CharField(max_length=255, null=False, blank=False, choices=TYPES, default='Host')
    project = models.ForeignKey(Project, null=True, blank=True, related_name='triggers', on_delete=models.CASCADE)
    silence_interval = models.IntegerField(null=False, blank=False, default=30)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s" % (self.name)
