from celery.decorators import task
from notifications.utils import send_mail_via_ses
import logging

logger = logging.getLogger(__name__)

@task(name = "process_trigger")
def process_trigger(entity_name, meta_info, value, project):
    """
    perfrom triggering based on triggers added in scenario
    """
    item_name = value.Item.name
    scenario = value.Item.scenario
    trigger = scenario.trigger
    try:
        action = trigger.Action
    except:
        ObjectDoesNotExist
        logger.debug("No trigger configured for trigger")
    else:
        if action.Triger_email_alert:
            subject = "Alert! {0} on {1} {2}".format(item_name, entity_name, meta_info)
            message = "{0} for {1} {2} is {3}\nAlert sent for scenario {4} {5} {6}\nKindly look into the issue\n\nYours Faithfully\nSleuth".format(item_name, entity_name, meta_info, value.data, item_name, scenario.condition, scenario.value_to_be_compared_with)
            logger.debug("sending mail alert -- {0}".format(message))
            send_mail_via_ses(project, subject, message) # to be converted into async task onces more action triggers are added

        if action.Trigger_pager_duty_event:
            pass

        if action.Trigger_sound_alarm:
            pass

        if action.Trigger_sms_alert:
            pass
