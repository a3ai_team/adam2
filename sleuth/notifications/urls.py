try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
    from django.conf.urls.defaults import *

from .views import action_list,action_delete,action_create,action_view,action_edit,trigger_list,trigger_delete,trigger_create,trigger_view,trigger_edit

# place app url patterns here
urlpatterns=[
    url(r'^action/list/$', action_list, name="action_list"),
    url(r'^action/delete/$', action_delete, name="action_delete"),
    url(r'^action/create/$', action_create, name='action_create'),
    url(r'^action/view/(?P<id>[\w-]+)$', action_view, name='action_view'),
    url(r'^action/edit/(?P<id>[\w-]+)$', action_edit, name='action_edit'),
    url(r'^trigger/list/$', trigger_list, name="trigger_list"),
    url(r'^trigger/delete/$', trigger_delete, name="trigger_delete"),
    url(r'^trigger/create/$', trigger_create, name='trigger_create'),
    url(r'^trigger/view/(?P<id>[\w-]+)$', trigger_view, name='trigger_view'),
    url(r'^trigger/edit/(?P<id>[\w-]+)$', trigger_edit, name='trigger_edit'),
]
