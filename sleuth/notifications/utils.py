from boto3 import Session
from celery.decorators import task
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from botocore.exceptions import ClientError
import logging

logger = logging.getLogger(__name__)

@task(name = "send_mail_ses")
def send_mail_via_ses(project, subject, message):
    """
    send an email via sms
    """

    session = Session(
    aws_access_key_id=project.aws_access_key_id,
    aws_secret_access_key=project.aws_secret_access_key,
    region_name='us-east-1')
    ses = session.client('ses')
    try:
        response = ses.send_email(
            Source=project.notification_from_email_address,
            Destination={
                'ToAddresses': [
                    project.notification_to_email_address,
                ],
                'CcAddresses': [

                ],
                'BccAddresses': [

                ]
            },
            Message={
                'Subject': {
                    'Data': subject,
                    'Charset': 'utf-8'
                },
                'Body': {
                    'Text': {
                        'Data': message,
                        'Charset': 'utf-8'
                    },
                }
            },
            ReplyToAddresses=[

            ],
        )
        logger.debug("sent mail for alert")
    except ClientError:
        logger.debug("mail not sent for incident")
