from .models import Trigger,Action_profile
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http import Http404
from django.shortcuts import render,redirect,get_object_or_404

from .forms import ActionForm,TriggerForm
from project.models import Project
# Create your views here.


@login_required
def action_list(request):
    """
    view to get list of all actions
    """
    kwargs = {}
    actions = Action_profile.objects.filter(**kwargs)
    paginator = Paginator(actions,10)
    page = request.GET.get('page')
    try:
        actions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        actions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        actions = paginator.page(paginator.num_pages)
    return render(request,"notifications/action_list.html",{'actions': actions})

@login_required
def action_delete(request):
    """
    view to delete multiple actions
    """
    if request.POST:
        try:
            list_delete=dict(request.POST)['action_selected']
        except:
            list_delete=[]
        actions=Action_profile.objects.filter(pk__in=list_delete)
        if actions is not None:
            actions.delete()
    return redirect(reverse_lazy('action_list'))

@login_required
def action_create(request):
    """
    view to create an action
    """
    form = ActionForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('action_list'))
    return render(request,"notifications/action_form.html",{'form':form})

@login_required
def action_view(request,id):
    """
    view for rendering action information
    """
    action = get_object_or_404(Action_profile,pk=id)
    form=ActionForm(request.POST or None, instance=action)
    return render(request,'notifications/action_form.html',{'form':form,'view':1,'edit':0,'action':action})

@login_required
def action_edit(request,id):
    """
    view to edit action instance
    """
    action=get_object_or_404(Action_profile,pk=id)
    form=ActionForm(request.POST or None, instance=action)
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('action_list'))
    return render(request,"notifications/action_form.html",{'form':form ,'view':0,'edit':1,'action':action})

@login_required
def trigger_list(request):
    """
    view to get list of all triggers
    """
    kwargs = {}
    kwargs['project__users'] = request.user.user
    if request.GET.get('project'):
        kwargs['project'] = Project.objects.get(pk=int(request.GET.get('project')))
    triggers = Trigger.objects.filter(**kwargs)
    paginator = Paginator(triggers,10)
    page = request.GET.get('page')
    try:
        triggers = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        triggers = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        triggers = paginator.page(paginator.num_pages)
    projects = Project.objects.filter(users=request.user.user)
    return render(request,"notifications/trigger_list.html",{'triggers': triggers,'projects':projects})

@login_required
def trigger_delete(request):
    """
    view to delete multiple triggers
    """
    if request.POST:
        try:
            list_delete=dict(request.POST)['trigger_selected']
        except:
            list_delete=[]
        triggers=Trigger.objects.filter(pk__in=list_delete)
        if triggers is not None:
            triggers.delete()
    return redirect(reverse_lazy('trigger_list'))

@login_required
def trigger_create(request):
    """
    view to create an trigger
    """
    form = TriggerForm(request.POST or None)
    try:
        form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    except:
        #raise when url hit by user is not valid
        raise Http404(" Sorry Page is invalid ")
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('trigger_list'))
    return render(request,"notifications/trigger_form.html",{'form':form})

@login_required
def trigger_view(request,id):
    """
    view for rendering trigger information
    """
    trigger = get_object_or_404(Trigger,pk=id)
    form=TriggerForm(request.POST or None, instance=trigger)
    try:
        form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    except:
        #raise when url hit by user is not valid
        raise Http404(" Sorry Page is invalid ")
    return render(request,'notifications/trigger_form.html',{'form':form,'view':1,'edit':0,'trigger':trigger})

@login_required
def trigger_edit(request,id):
    """
    view to edit trigger instance
    """
    trigger=get_object_or_404(Trigger,pk=id)
    form=TriggerForm(request.POST or None, instance=trigger)
    try:
        form.fields['project'].queryset=Project.objects.filter(users=request.user.user)
    except:
        #raise when url hit by user is not valid
        raise Http404(" Sorry Page is invalid ")
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('trigger_list'))
    return render(request,"notifications/trigger_form.html",{'form':form ,'view':0,'edit':1,'trigger':trigger})

