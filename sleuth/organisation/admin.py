# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import User, Organisation


class UserAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'name',
        'email',
        'phone',
        'alerts_enabled',
        'created_at',
        'updated_at',
    )
    list_filter = ('alerts_enabled', 'created_at', 'updated_at')
    search_fields = ('name', 'email', 'phone')

    def get_queryset(self, request):
        """
        list all user in working in same organistaions
        """
        query = super(UserAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(organisations__users=request.user.user).distinct().order_by('-created_at')



admin.site.register(User, UserAdmin)


class OrganisationAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'created_at',
        'updated_at',
    )
    def get_queryset(self, request):
        """
        filter Organisation based on logged in user
        """
        query = super(OrganisationAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(users=request.user.user).order_by('-created_at')


admin.site.register(Organisation, OrganisationAdmin)
