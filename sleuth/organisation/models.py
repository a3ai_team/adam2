from __future__ import unicode_literals
from django.contrib.auth.models import User as user
from django.db.models.signals import post_save
import logging

logger = logging.getLogger(__name__)

from django.db import models

class User(models.Model):
    """
    Represents an user belonging to the organistaion
    """
    user = models.OneToOneField(user, null=False, blank=False, on_delete=models.CASCADE, related_name='user')
    name = models.CharField(max_length=255, null=True, blank=False)
    email = models.EmailField(null=True, blank=False)
    phone = models.IntegerField(null=True, blank=False)
    alerts_enabled = models.BooleanField(null=False, blank=False, default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return "%s" % (self.user)

def create_user(sender, instance, created, **kwargs):
    """
    create a user when a django  user is created
    """
    user, created = User.objects.get_or_create(user=instance)
    user.name = instance.first_name
    user.email = instance.email
    user.save()
    if created:
        logger.debug("created user for org")
    else:
        logger.debug("updated user for org")


post_save.connect(create_user, sender=user)

class Organisation(models.Model):
    """
    Represents an organistaion
    """
    name = models.CharField(max_length=255, null=True, blank=False)
    users = models.ManyToManyField(User, blank=True, related_name='organisations')
    owners = models.ManyToManyField(User, blank=True, related_name='owned_organisations')
    admins = models.ManyToManyField(User, blank=True, related_name='administered_organisations')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    def __unicode__(self):
        return "%s" % (self.name)
