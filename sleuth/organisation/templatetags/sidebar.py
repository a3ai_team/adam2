from organisation.models import Organisation
from django import template
register = template.Library()

@register.simple_tag
def show_sidebar(request):
    """
    templatetag for organisation in sidebar of global_layout
    """
    org=Organisation.objects.filter(users=request.user.user)
    return org
