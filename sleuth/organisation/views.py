from django.shortcuts import render,render_to_response,redirect
from registration.backends.simple.views import RegistrationView
from registration import signals
from django.conf import settings
from django.contrib.auth import authenticate,login
from organisation.models import Organisation,User
from django.template import RequestContext
from django.views.generic import View
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm

# Create your views here.
class SleuthRegistrationView(RegistrationView):

    disallowed_url="/"
    success_url="/"
    def registration_allowed(self):
        if bool(self.request.session.keys()):
            return getattr(settings, 'REGISTRATION_OPEN', False)
        else:
            return getattr(settings, 'REGISTRATION_OPEN', True)

    def register(self, form):
        new_user = form.save()
        username_field = getattr(new_user, 'USERNAME_FIELD', 'username')
        new_user = authenticate(
            username=getattr(new_user, username_field),
            password=form.cleaned_data['password1']
        )

        login(self.request, new_user)
        signals.user_registered.send(sender=self.__class__,
                                     user=new_user,
                                     request=self.request)
        organization=self.request.POST.get('organization')

        if Organisation.objects.filter(name=organization).exists():
            org=Organisation.objects.get(name=organization)
        else:
            org=Organisation.objects.create(name=organization)

        user_obj=User.objects.get(user__username=new_user)
        org.users.add(user_obj.pk)
        org.save()
        return new_user

def login_view(request):
    form=LoginForm(request.POST or None)
    next_url=request.GET.get('next')
    if form.is_valid():
        username=form.cleaned_data.get('username')
        password=form.cleaned_data.get('password')
        next_url=request.POST.get('next')
        user=authenticate(username=username, password=password)
        if user is not None:
            login(request,user)
            if next_url:
                return redirect(next_url)
            else:
                return redirect('dashboard')
    if bool(request.session.keys()):
       return redirect('dashboard')
    return render(request,'registration/login.html',{'form':form,'next':next_url })


def logout_view(request):
    logout(request)
    return redirect('login_view')
