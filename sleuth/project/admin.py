# -*- coding: utf-8 -*-
from django.contrib import admin
from .models import Project, Incident
from organisation.models import Organisation,User
from .filters import ProjectFieldFilter
class ProjectAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Basic Info', { 'fields' : ('name', 'organisation', 'aws_access_key_id','aws_secret_access_key','notification_from_email_address','notification_to_email_address', 'users')}),
        ('Mail and Pager duty Integration', {
            'classes': ('grp-collapse','grp-open'),
            'fields': ('pager_duty_api_key',
                'sms_service_api_key',
                'mail_service_api_key'),

        }),
    )
    filter_horizontal=('users',)
    list_display = (
        'name',
        'organisation',
        'aws_access_key_id',
        'aws_secret_access_key',
        'notification_from_email_address',
        'notification_to_email_address',
        'pager_duty_api_key',
        'sms_service_api_key',
        'mail_service_api_key',
    )
    list_filter = ('created_at', 'updated_at')
    search_fields = ('name',)
    date_hierarchy = 'created_at'

    def get_queryset(self, request):
        #filter project based on logged in user
        query = super(ProjectAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(users=request.user.user).order_by('-created_at')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all Organisation where user belongs
        """
        if db_field.name == "organisation" and not request.user.is_superuser:
            kwargs["queryset"] = Organisation.objects.filter(users=request.user.user)
        return super(ProjectAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)



admin.site.register(Project, ProjectAdmin)


class IncidentAdmin(admin.ModelAdmin):
    list_display = (
        'project',
        'state',
        'details',
        'created_at',
        'updated_at',
        'acknowledged_at',
        'resolved_at',
    )
    list_filter = (
         ProjectFieldFilter,
        'created_at',
        'updated_at',
        'acknowledged_at',
        'resolved_at',
    )
    readonly_fields = ('Type', 'pager_duty_incident_id', 'priority')
    date_hierarchy = 'created_at'

    def get_queryset(self, request):
        #filter incident based on logged in user
        query = super(IncidentAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return query.filter().order_by('-created_at')
        return query.filter(project__users=request.user.user).order_by('-created_at')

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        """
        List all project where user belongs
        """
        if db_field.name == "project" and not request.user.is_superuser:
            kwargs["queryset"] = Project.objects.filter(users=request.user.user)
        return super(IncidentAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(Incident, IncidentAdmin)
