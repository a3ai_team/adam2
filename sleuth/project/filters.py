from django.contrib import admin
from .models import Project
from django.contrib.admin import SimpleListFilter

class ProjectFieldFilter(SimpleListFilter):
    """
    Define Filters for project in filter option
    """
    title = 'Project'
    parameter_name = 'Project'

    def lookups(self, request, model_admin):
        if request.user.is_superuser:
            project=Project.objects.filter()
        else:
            project = Project.objects.filter(users=request.user.user)
        return [(p['id'],p['name']) for p in project.values()]

    def queryset(self, request,queryset):
        if self.value():
            return queryset.filter(project__id=self.value())
        else:
            return queryset
