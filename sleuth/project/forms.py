from django import forms
from .models import Project
from organisation.models import Organisation,User
# place form definition here
class ProjectForm(forms.ModelForm):
    #organisation = forms.ModelChoiceField(queryset=Organisation.objects.filter(users=request.user.user))
    class Meta:
        model=Project
        fields=['id','name','organisation','users','pager_duty_api_key','aws_access_key_id','aws_secret_access_key','mail_service_api_key','notification_from_email_address','notification_to_email_address']
