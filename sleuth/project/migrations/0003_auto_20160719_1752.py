# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-19 12:22
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('project', '0002_incident_details'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='aws_access_key_id',
            field=models.CharField(default=b'fsdfs', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='project',
            name='aws_secret_access_key',
            field=models.CharField(default=datetime.datetime(2016, 7, 19, 12, 22, 22, 156156, tzinfo=utc), max_length=255),
            preserve_default=False,
        ),
    ]
