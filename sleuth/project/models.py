from __future__ import unicode_literals
from django.db import models
from organisation.models import Organisation, User
import uuid
from django_extensions.db.fields import UUIDField

class Project(models.Model):
    """
    Denotes a Project eg. FAME
    """
    name = models.CharField(max_length=255, null=False, blank=False)
    organisation = models.ForeignKey(Organisation, null=True, blank=False, on_delete=models.CASCADE, related_name='projects')
    users = models.ManyToManyField(User, blank=True, related_name='projects')
    pager_duty_api_key = models.CharField(max_length=255, null=True, blank=True)
    sms_service_api_key = models.CharField(max_length=255, null=True, blank=True)
    mail_service_api_key = models.CharField(max_length=255, null=True, blank=True)
    aws_access_key_id = models.CharField(max_length=255, null=False, blank=False)
    aws_secret_access_key = models.CharField(max_length=255, null=False, blank=False)
    notification_from_email_address = models.EmailField(null=True, blank=False)
    notification_to_email_address = models.EmailField(null=True, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "%s" % (self.name)

class Incident(models.Model):
    """
    Denotes an incident that gets created post an event trigger
    """
    project = models.ForeignKey(Project, null=True, on_delete=models.SET_NULL, related_name='incidents')
    guid = UUIDField(default=uuid.uuid4, primary_key=True)
    TRIGGERED = 'Triggered'
    ACKNOWLEDGED = 'Acknowledged'
    RESOLVED = 'Resolved'
    STATES = (
        (TRIGGERED, 'Triggered'),
        (ACKNOWLEDGED, 'Acknowledged'),
        (RESOLVED, 'Resolved')
    )
    state = models.CharField(max_length=255, null=False, blank=False, choices=STATES, default='Triggered')

    HOST = 'Host'
    APPLICATION = 'Application'
    API_CHECK = 'API_check'
    SERVICE = 'Service'
    CRON = 'Cron_check'
    TYPES = (
        (HOST, 'HOST'),
        (APPLICATION, 'APPLICATION'),
        (API_CHECK, 'API_check'),
        (SERVICE, 'SERVICE'),
        (CRON, 'CRON_check')
    )
    Type = models.CharField(max_length=255, null=False, blank=False, choices=TYPES, default='HOST')
    PRIORITIES = (
        ('LOW', 'LOW'),
        ('MEDIUM', 'MEDIUM'),
        ('HIGH', 'HIGH')
    )
    item = models.CharField(max_length=255, null=True, blank=False) # contains defination of item in the form of a dict eg. {"name":"CPU_LOAD", "id":4}

    pager_duty_incident_id = models.CharField(max_length=255, null=True, blank=True)
    priority = models.CharField(max_length=255, null=False, blank=False, choices=PRIORITIES, default='LOW')
    details = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    acknowledged_at = models.DateTimeField(auto_now=True)
    resolved_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "incident id %s" % (self.pk)
