from project.models import Incident,Project
from django import template
register = template.Library()

@register.simple_tag
def count_triggered_incident(request):
    """
    templatetag for triggered incident
    """
    count=Incident.objects.filter(state='Triggered',project__users=request.user.user).count()
    return count

@register.simple_tag
def count_triggered_incident_local(request):
    """
    templatetag for triggered incident
    """
    kwargs = {}
    kwargs['project__users'] = request.user.user
    kwargs['state'] = 'Triggered'
    if request.GET.get('project'):
        kwargs['project'] = Project.objects.get(id=int(request.GET.get('project')))
    count=Incident.objects.filter(**kwargs).count()
    return count

@register.simple_tag
def count_resolved_incident(request):
    """
    templatetag for resolved incident
    """
    kwargs = {}
    kwargs['project__users'] = request.user.user
    kwargs['state'] = 'Resolved'
    if request.GET.get('project'):
        kwargs['project'] = Project.objects.get(id=int(request.GET.get('project')))
    count=Incident.objects.filter(**kwargs).count()
    return count

@register.simple_tag
def count_acknowledged_incident(request):
    """
    templatetag for acknowledged incident
    """
    kwargs = {}
    kwargs['project__users'] = request.user.user
    kwargs['state'] = 'Acknowledged'
    if request.GET.get('project'):
        kwargs['project'] = Project.objects.get(id=int(request.GET.get('project')))
    count=Incident.objects.filter(**kwargs).count()
    return count

@register.simple_tag
def count_all_incident(request):
    """
    templatetag for all incident
    """
    kwargs = {}
    kwargs['project__users'] = request.user.user
    if request.GET.get('project'):
        kwargs['project'] = Project.objects.get(id=int(request.GET.get('project')))
    count=Incident.objects.filter(**kwargs).count()
    return count
