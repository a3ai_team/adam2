try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
    from django.conf.urls.defaults import *

from .views import project_create,project_delete,project_list,project_user,project_view,project_edit

urlpatterns=[
        url(r'^list/(?P<org>[\w-]+)$',project_list,name='project_list'),
        url(r'^users/(?P<org>[\w-]+)/(?P<project>[\w-]+)$',project_user,name='project_user'),
        url(r'^create/(?P<org>[\d-]+)$',project_create,name='project_create'),
        url(r'^delete/$', project_delete, name='project_delete'),
        url(r'^details/(?P<org>[\w-]+)/(?P<project>[\w-]+)$',project_view,name='project_view'),
        url(r'^edit/(?P<org>[\w-]+)/(?P<project>[\w-]+)$',project_edit,name='project_edit'),
]
