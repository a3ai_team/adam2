from django.views.generic import View
from django.http import Http404
from django.shortcuts import render,redirect,get_object_or_404
import logging
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from project.models import Project, Incident
from organisation.models import Organisation,User
from django.http import JsonResponse
from django.views.generic import ListView
from .forms import ProjectForm
from django.core.urlresolvers import reverse_lazy
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from inventory.models import Region,Host,Application
from monitor.models import API_check,Cron_check,Item
from django.db.models import Count
logger = logging.getLogger(__name__)


@login_required
def project_list(request,org):
    """
    listing the project in particular org
    """
    try:
        org_name=Organisation.objects.get(pk=org)
    except:
        org_name=""
    org_list=Organisation.objects.filter(users=request.user.user)
    if org_name in org_list:
        projects_list=Project.objects.filter(users=request.user.user, organisation__pk = org)
        paginator = Paginator(projects_list, 7)
        page = request.GET.get('page')
        try:
            projects = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            projects = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            projects = paginator.page(paginator.num_pages)
    else:
        raise Http404(str(request.user)+ " is not in Organisation "+org_name.name+ " or Organisation is not available")
    return render(request,"project/project_list.html",{'org':org_name,"projects":projects})

@login_required
def project_create(request ,org):
    """
    view for creating projects
    """
    form = ProjectForm(request.POST or None)
    try:
        form.fields['users'].queryset=User.objects.filter(organisations__id=org)
        org=Organisation.objects.get(pk=org,users=request.user.user)
    except:
        #raise when url hit by user is not valid
        raise Http404(" Sorry Page is invalid ")
    if request.POST:
        if form.is_valid():
            form.save()
            return redirect(reverse_lazy('project_list',args=[org.id]))
    return render(request,"project/project_form.html",{'form':form ,'org':org})

@login_required
def project_delete(request):
    """
    view for deleting project
    """
    org=request.POST.get('org')
    try:
        org=Organisation.objects.get(name=org)
    except:
        raise Http404("Wrong Organisation")
    if request.POST:
        try:
            list_delete=dict(request.POST)['project_selected']
        except:
            list_delete=[]
        project=Project.objects.filter(pk__in=list_delete)
        if project is not None:
            if (request.user.is_superuser) or (request.user.user in User.objects.filter()):
                project.delete()
    return redirect(reverse_lazy('project_list' ,args=[org.id]))




@method_decorator(login_required, name='dispatch')
class DashboardView(View):
    def get(self, request, *args, **kwargs):
        #project_list = get_sidebar_context()
        project_list=Project.objects.filter(users=request.user.user)
        org_list=Organisation.objects.filter(users=request.user.user)
        host_list=Host.objects.filter(region__project__users=request.user.user)
        api_list=API_check.objects.filter(project__users=request.user.user)
        cron_list=Cron_check.objects.filter(project__users=request.user.user)
        item_list=Item.objects.filter(project__users=request.user.user)
        app_list=Application.objects.filter(host__region__project__users=request.user.user)
        top_host_list=Host.objects.filter(region__project__users=request.user.user).annotate(num_applications__host=Count('applications__host')).order_by('-num_applications__host')[:5]
        context = {'projects': project_list,'organisations':org_list,'apichecks':api_list,'cronchecks':cron_list,'items':item_list,'hosts':host_list,'apps':app_list,'top_hosts':top_host_list,'view':1}
        return render(request, 'dashboard.html', context)

@method_decorator(login_required, name='dispatch')
class ArchitectureView(View):
    def get(self, request, *args, **kwargs):
        project_list = get_sidebar_context()
        context = {'projects': project_list}
        return render(request, 'architecture.html', context)



def update_incident_view(request, incident_guid=None):
    """
    changes the incident state
    """
    if request.method == 'GET' and (incident_guid is not None):
        #request.GET.get('key', None)
        action = request.GET.get('action', None)
        if (action is None) or (action not in ['Acknowledged', 'Resolved']):
            data = {"code":"400", "message":"bad request"}
            return JsonResponse(data, status=400)
        else:
            incidents = Incident.objects.filter(guid=incident_guid)
            if len(incidents)==0:
                data = {"code":"404", "message":"resouce not found"}
                return JsonResponse(data, status=404)
            else:
                incident = incidents[0]
                current_incident_state = incident.state
                if current_incident_state == 'Resolved':
                    data = {"code":"200", "message":"incident already resolved, nothing to do.."}
                    return JsonResponse(data, status=200)
                else:
                    incident.state = action
                    incident.save()
                    data = {"code":"200", "message":"updated incident"}
                    return JsonResponse(data, status=200)

    else:
        data = {"code":"400", "message":"bad request"}
        return JsonResponse(data, status=400)

@login_required
def project_user(request,org,project):
    """
    show users for particular project
    """
    try:
        org_name=Organisation.objects.get(pk=org)
    except:
        org_name=""
    org_list=Organisation.objects.filter(users=request.user.user)
    if org_name in org_list:
        try:
            project=Project.objects.get(pk=project,users=request.user.user)
            user_list=User.objects.filter(projects__name=project)
            paginator = Paginator(user_list, 7)
            page = request.GET.get('page')
            try:
                users = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                users = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                users = paginator.page(paginator.num_pages)
        except:
            raise Http404("Wrong url ")
    return render(request,"project/project_users.html",{'org':org_name,'project':project,'users':users})


@login_required
def project_edit(request,org,project):
    """
    Read project information
    """
    try:
        org_name=Organisation.objects.get(pk=org)
    except:
        raise Http404("Invalid Page")
    org_list=Organisation.objects.filter(users=request.user.user)
    if org_name in org_list:
        #check whether project is available or not
        project=get_object_or_404(Project,pk=project,users=request.user.user)
        form=ProjectForm(request.POST or None, instance=project)
        form.fields['users'].queryset=User.objects.filter(organisations__id=org)
        if request.POST:
            if form.is_valid():
                form.save()
                return redirect(reverse_lazy('project_list',args=[org_name.id]))
    return render(request,"project/project_form.html",{'form':form ,'org':org_name,'view':0,'edit':1,'project':project})


@login_required
def project_view(request,org,project):
    """
    Read project information
    """
    try:
        org_name=Organisation.objects.get(pk=org)
    except:
        raise Http404("Invalid Page")
    org_list=Organisation.objects.filter(users=request.user.user)
    if org_name in org_list:
        #check whether project is available or not
        project=get_object_or_404(Project,pk=project,users=request.user.user)
        form=ProjectForm(request.POST or None, instance=project)
        form.fields['users'].queryset=User.objects.filter(organisations__id=org)
    return render(request,"project/project_form.html",{'form':form ,'org':org_name,'view':1,'edit':0,'project':project})

def incident_list(request):
    """
    fetch incidents list
    """
    kwargs = {}
    kwargs['project__users__user'] = request.user
    if request.GET.get('state'):
        kwargs['state'] = request.GET.get('state')
    if request.GET.get('project'):
        kwargs['project'] = Project.objects.get(id=int(request.GET.get('project')))
    incident_list = Incident.objects.filter(**kwargs).order_by('-created_at')
    paginator=Paginator(incident_list,10)
    page=request.GET.get('page')
    try:
        incidents = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        incidents = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        incidents = paginator.page(paginator.num_pages)

    projects = Project.objects.filter(users=request.user.user)
    return render(request,"project/incident_list.html",{"incidents": incidents, 'projects': projects})

def update_delete_incident(request):
    """
    update state or delete multiple incidents
    """
    try:
        action = request.GET.get('action')
    except:
        action = ''
    try:
        incident_list = request.GET.get('incidents')
    except:
        incident_list = []
    incidents = Incident.objects.filter(pk__in=incident_list.split(","))
    if incidents is not None:
        if action == 'Delete':
            incidents.delete()
        elif action == 'Resolve':
            incidents.update(state="Resolved")
        elif action == 'Acknowledge':
            incidents.filter(state="Triggered").update(state="Acknowledged")
    return redirect(reverse_lazy('incident_list'))
