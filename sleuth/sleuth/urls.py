"""sleuth URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from project.views import DashboardView, ArchitectureView
from organisation.views import SleuthRegistrationView,login_view,logout_view
from django.contrib.auth import views as auth_views
from inventory.views import update_region_view
from project.views import update_incident_view,incident_list,update_delete_incident
from monitor.views import update_cron





urlpatterns = [
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^accounts/login/$', login_view, name='login_view'),
    url(r'^accounts/logout/$',logout_view,name='logout_view'),
    url('^projects/',include('project.urls')),
    url('^inventory/',include('inventory.urls')),
    url('^monitor/', include('monitor.urls')),
    url(r'^accounts/register/$', SleuthRegistrationView.as_view(), name='registration_register'),
    url('^accounts/', include('registration.backends.simple.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^$', DashboardView.as_view(), name='dashboard'),
    url(r'^architecture', ArchitectureView.as_view(), name='architecture'),
    url(r'^update/region/(?P<region_guid>[\w-]+)/$', update_region_view),
    url(r'^update/incident/(?P<incident_guid>[\w-]+)/$', update_incident_view),
    url(r'^update/cron/(?P<cron_check_guid>[\w-]+)/$', update_cron),
    url(r'^incident/list/$', incident_list, name='incident_list'),
    url(r'^incident/action/$', update_delete_incident, name='update_delete_incident'),
    url('^notifications/', include('notifications.urls')),
]
