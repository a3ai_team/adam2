#!/bin/bash
NAME="sleuth"
DJANGODIR=/home/sleuth/workspace/sleuth/sleuth # Django project directory
DJANGO_SETTINGS_MODULE=sleuth.settings             # which settings file should Django use
LOGFILE=/var/log/celerybeat.log

echo "Starting $NAME celerybeat"

# Activate the virtual environment
cd $DJANGODIR
source /home/sleuth/workspace/envs/sleuth/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH


exec python manage.py celery beat --loglevel=INFO --logfile=$LOGFILE --pidfile=/tmp/celerybeat.pid
